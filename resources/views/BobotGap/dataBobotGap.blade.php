<!DOCTYPE html>

<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('Template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('Template.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Data Bobot Gap</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Data Bobot Gap</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="card card-info card-outline">
                    <div class="card-header">
                        <div class="card-tools">
                            <a href="{{ route('create-bobot-gap') }}" class="btn btn-success">Tambah Data <i
                                    class="fas fa-plus-square"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead class="table table-bordered">
                                <tr>
                                    <th>#</th>
                                    <th>Selisih</th>
                                    <th>Nilai Bobot</th>
                                    <th>Keterangan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            @foreach ($dtBobotGap as $items => $item)
                            <tbody class="table table-bordered">
                                <tr>
                                    {{-- <th>{{ ++$items }}</th> --}}
                                    <th>{{ $dtBobotGap->firstItem() + $items }}</th>
                                    <td>{{ $item->selisih }} </td>
                                    <td>{{ $item->bobot }}</td>
                                    <td>{{ $item->keterangan }}</td>
                                    <td>
                                        <a href="{{ route('edit-bobot-gap', $item->id) }}"><i
                                                class="fas fa-edit"></i></a> |
                                        <a onclick="return confirm('Are you sure?')"
                                            href="{{ route('delete-bobot-gap', $item->id) }}">
                                            <i class="fas fa-trash-alt" style="color:red"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center">
                            {!! $dtBobotGap->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        @include('Template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    @include('Template.script')
    @include('sweetalert::alert')
</body>

</html>
