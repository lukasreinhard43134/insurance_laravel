<!DOCTYPE html>

<html lang="en">

<head>
    @include('Template.head')

</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('Template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('Template.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Data Nilai</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Data Nilai</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="card card-info card-outline">
                    <div class="card-header">
                        <div class="card-tools">
                            {{-- <a href="{{ route('create-produk') }}" class="btn btn-success">Tambah Data <i
                                class="fas fa-plus-square"></i></a> --}}
                        </div>
                    </div>
                    {{-- <form action="{{ route('simpan-data-nilai') }}" method="POST"> --}}
                    {{-- {{ csrf_field() }} --}}
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead class="table table-bordered">
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">Nama Produk</th>
                                    <th>Aksi</th>
                                    {{-- <th colspan="5" style="text-align: center;">Masukkan Nilai</th> --}}
                                </tr>
                                {{-- <tr>
                                        @foreach ($getSubKriteria as $item)
                                        <th>{{ $item->nama }}</th>
                                @php
                                $i=App\Models\Kategori::all()->where('sub_kriteria_nama',$item->nama);
                                echo($item->nama)
                                @endphp
                                @endforeach
                                </tr> --}}
                            </thead>
                            @foreach ($getProduk as $tot => $item)
                            <tbody>
                                <tr>
                                    {{-- <td>{{ $loop->iteration }}</td> --}}
                                    <td>{{ $getProduk->firstItem() + $tot }}</td>
                                    <td>
                                        <input type="hidden" name="nama[]" />{{ $item->nama_produk }}
                                    </td>
                                    <td>
                                        <a href="#"><i class="fas fa-eye" data-id="tol" data-toggle="modal"
                                                data-target="#modal-default2{{ $item->id_produk }}"></i>
                                        </a> |
                                        <a href="#"><i class="fas fa-plus-square" data-id="tol" data-toggle="modal"
                                                data-target="#modal-default{{ $item->id_produk }}"
                                                style="color:green"></i></a>
                                        <div class="modal fade" id="modal-default{{ $item->id_produk }}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">{{ $item->nama_produk }}
                                                        </h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{ route('simpan-data-nilai') }}" method="post">
                                                            {{ csrf_field() }}
                                                            <div class="form-group row">
                                                                <label for="inputEmail3"
                                                                    class="col-sm-2 col-form-label">Nama Produk</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" id="produk"
                                                                        value="{{ $item->nama_produk }}" name="produk"
                                                                        readonly />
                                                                </div>
                                                            </div>
                                                            @foreach ($subKriteria as $datas )
                                                            {{-- @if($datas->tipe_input == 'input')
                                                            <div class="form-group row">
                                                                <label for="inputEmail3"
                                                                    class="col-sm-2 col-form-label">{{ $datas->nama }}</label>
                                                            <div class="col-sm-10">
                                                                <input type="number"
                                                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                    class="form-control" id="{{ $datas->nama }}"
                                                                    maxLength="2" name="{{ $datas->nama }}"
                                                                    placeholder="masukkan jumlah {{ $datas->nama }}"
                                                                    required>
                                                                <span
                                                                    style="color: red;">@error('{{ $datas->nama }}'){{ $message }}@enderror</span>
                                                            </div>
                                                    </div>
                                                    @elseif ($datas->tipe_input == 'input_currency')
                                                    <div class="form-group row">
                                                        <label for="inputEmail3"
                                                            class="col-sm-2 col-form-label">{{ $datas->nama }}</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control input-currency"
                                                                id="{{ $datas->nama }}" name="{{ $datas->nama }}"
                                                                placeholder="Rp" NumberOnly="true"
                                                                type-currency="IDR" />
                                                            <span
                                                                style="color: red;">@error('{{ $datas->nama }}'){{ $message }}@enderror</span>
                                                        </div>
                                                    </div>
                                                    @elseif ($datas->tipe_input == 'pilihan')
                                                    <div class="form-group row">
                                                        <label for="inputEmail3"
                                                            class="col-sm-2 col-form-label">{{ $datas->nama }}</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" name="{{ $datas->nama }}"
                                                                id="{{ $datas->nama }}">
                                                                <option>-- pilih salah satu --</option>
                                                                @php
                                                                $i=App\Models\Kategori::all()->where('sub_kriteria_nama',$datas->nama);
                                                                @endphp
                                                                @foreach ($i as $datass)
                                                                <option value={{ $datass->nilai }}>
                                                                    {{ $datass->nama }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @endif --}}
                                                    @if($datas->tipe_input_2 == 'pilihan')
                                                    <div class="form-group row">
                                                        <label for="inputEmail3"
                                                            class="col-sm-2 col-form-label">{{ $datas->nama }}</label>
                                                        <div class="col-sm-10">
                                                            {{-- <input type="text" class="form-control" id="nama" name="nama"
                                                                        placeholder="masukkan nama kriteria"> --}}
                                                            <select class="form-control" name="{{ $datas->nama }}"
                                                                id="{{ $datas->nama }}">
                                                                <option>-- pilih salah satu --</option>
                                                                @php
                                                                $i=App\Models\Kategori::all()->where('sub_kriteria_nama',$datas->nama);
                                                                @endphp
                                                                @foreach ($i as $datass)
                                                                <option value={{ $datass->nilai }}>
                                                                    {{ $datass->nama }}</option>
                                                                @endforeach
                                                            </select>
                                                            {{-- <span style="color: red;">@error('subKriteria'){{ $message }}@enderror</span>
                                                            --}}
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-outline-danger"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit"
                                                            class="btn btn-outline-primary">Submit</button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                    </div>
                    <div class="modal fade" id="modal-default2{{ $item->id_produk }}">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">{{ $item->nama_produk }}
                                    </h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($subKriteria as $datas )
                                    @php
                                    $i=App\Models\NilaiProduk::all()->where('nama_produk',$item->nama_produk)->where('nama',
                                    $datas->nama);
                                    @endphp
                                    <div class="form-group row">
                                        <label for="inputEmail3"
                                            class="col-sm-6 col-form-label">{{ $datas->nama }}</label>
                                        <div class="col-sm-6">
                                            @foreach ($i as $itemss)
                                            @php
                                            $j=App\Models\Kategori::all()->where('sub_kriteria_nama',$datas->nama)->where('nilai',
                                            $itemss->nilai);
                                            @endphp
                                            @foreach ($j as $itemsss)
                                            <input type="text" value="{{ $itemsss->nama }}" disabled>
                                            @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{-- <button type="button" class="btn btn-primary">Save
                                                            changes</button> --}}
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    </td>
                    </tr>
                    </tbody>
                    @endforeach
                    </table>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center">
                        {!! $getProduk->links() !!}
                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    @include('Template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    @include('Template.script')
    @include('sweetalert::alert')
    <script>
        document.querySelectorAll('input[type-currency="IDR"]').forEach((element) => {
            element.addEventListener('keyup', function(e) {
                let cursorPostion = this.selectionStart;
                let value = parseInt(this.value.replace(/[^,\d]/g, ''));
                let originalLenght = this.value.length;
                if (isNaN(value)) {
                this.value = "";
                } else {
                this.value = value.toLocaleString('id-ID', {
                    currency: 'IDR',
                    style: 'currency',
                    minimumFractionDigits: 0
                });
                cursorPostion = this.value.length - originalLenght + cursorPostion;
                this.setSelectionRange(cursorPostion, cursorPostion);
                }
            });
        });
    </script>


</body>

</html>
