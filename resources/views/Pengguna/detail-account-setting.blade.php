<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Detail Account</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Detail Account</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">

                                    </h5>
                                </div>
                                <div class="card-body">
                                    @if ( Session::get('isRole')=="admin")
                                        <form action="{{ route('update-account', $getData[0]->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input disabled type="text" class="form-control" id="emails" name="email"
                                                        placeholder="masukkan email " value="{{ $getData[0]->email }}">
                                                    <span style="color: red;">@error('email'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Hak Akses</label>
                                                <div class="col-sm-10">
                                                    <select disabled class="form-control" name="role" id="role">
                                                        {{-- <option value="$getData[0]->name">{{ $getData[0]->name }}</option> --}}
                                                        @if ($getData[0]->role === 'admin')
                                                        <option value="admin">admin</option>
                                                        <option value="customer">customer</option>
                                                        @elseif($getData[0]->role === 'customer')
                                                        <option value="customer">customer</option>
                                                        <option value="admin">admin</option>
                                                        @endif
                                                    </select>
                                                    <span style="color: red;">@error('role'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullnames" name="fullname"
                                                        placeholder="masukkan nama " value="{{ $getData[0]->name }}">
                                                    <span style="color: red;">@error('fullname'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="passwords" name="password"
                                                        placeholder="masukkan password " value="{{ $getData[0]->password }}">
                                                    <span style="color: red;">@error('password'){{ $message }}@enderror</span>
                                                </div>
                                            </div> --}}
                                            <div class="form-group">
                                                <a class="btn btn-outline-danger" href="{{ route('account-setting', auth()->user()->name) }}">Back</a>
                                                <button type="submit" onclick="return confirm('Please Login after submit updated. Are you sure?')" class="btn btn-outline-primary">Edit Data</button>
                                            </div>
                                        </form>
                                    @elseif(Session::get('isRole')=="customer")
                                        <form action="{{ route('update-account', $getData[0]->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input disabled type="text" class="form-control" id="emails" name="email"
                                                        placeholder="masukkan email " value="{{ $getData[0]->email }}">
                                                    <span style="color: red;">@error('email'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Hak Akses</label>
                                                <div class="col-sm-10">
                                                    <select disabled class="form-control" name="role" id="role">
                                                        {{-- <option value="$getData[0]->name">{{ $getData[0]->name }}</option> --}}
                                                        @if ($getData[0]->role === 'admin')
                                                        <option value="admin">admin</option>
                                                        <option value="customer">customer</option>
                                                        @elseif($getData[0]->role === 'customer')
                                                        <option value="customer">customer</option>
                                                        <option value="admin">admin</option>
                                                        @endif
                                                    </select>
                                                    <span style="color: red;">@error('role'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="fullnames" name="fullname"
                                                        placeholder="masukkan nama " value="{{ $getData[0]->name }}">
                                                    <span style="color: red;">@error('fullname'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Alamat</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="alamats" name="alamat"
                                                        placeholder="masukkan alamat " value="{{ $getData[0]->alamat }}">
                                                    <span style="color: red;">@error('alamat'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">No Handphone</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="no_hps" name="no_hp"
                                                        placeholder="masukkan no handphone " value="{{ $getData[0]->no_handphone }}">
                                                    <span style="color: red;">@error('no_hp'){{ $message }}@enderror</span>
                                                </div>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="passwords" name="password"
                                                        placeholder="masukkan password " value="{{ $getData[0]->password }}">
                                                    <span style="color: red;">@error('password'){{ $message }}@enderror</span>
                                                </div>
                                            </div> --}}
                                            <div class="form-group">
                                                <a class="btn btn-outline-danger" href="{{ route('account-setting', auth()->user()->name) }}">Back</a>
                                                <button type="submit" onclick="return confirm('Please Login after submit updated. Are you sure?')" class="btn btn-outline-primary">Edit Data</button>
                                            </div>
                                        </form>
                                    @endif
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-center">
                                        {{-- {!! $dtProduk->links() !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    @include('sweetalert::alert')
    <script>
        document.querySelectorAll('input[type-currency="IDR"]').forEach((element) => {
            element.addEventListener('keyup', function(e) {
                let cursorPostion = this.selectionStart;
                let value = parseInt(this.value.replace(/[^,\d]/g, ''));
                let originalLenght = this.value.length;
                if (isNaN(value)) {
                this.value = "";
                } else {
                this.value = value.toLocaleString('id-ID', {
                    currency: 'IDR',
                    style: 'currency',
                    minimumFractionDigits: 0
                });
                cursorPostion = this.value.length - originalLenght + cursorPostion;
                this.setSelectionRange(cursorPostion, cursorPostion);
                }
            });
        });
    </script>

</body>

</html>
