<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Pendaftaran Nasabah</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                {{-- <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Starter Page</li> --}}
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">
                                        Silahkan masukkan data diri anda dengan benar.
                                        Data ini hanya untuk keperluan Adminitrasi untuk dapat melakukan
                                        pemilihan Produk Asuransi pada aplikasi ini.
                                    </h5>
                                </div>
                                <form action="{{ route('postRegister') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="card-body">
                                        <div class="input-group mb-3">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama
                                                Lengkap</label>
                                            <input type="text" name="fullname" class="form-control"
                                                placeholder="masukkan nama lengkap">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            </div>
                                            <div class="col-sm-5">
                                                <span
                                                    style="color: red;">@error('fullname'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="input-group mb-3">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Alamat</label>
                                            <textarea type="text" class="form-control" id="alamat" name="alamat">
                                            </textarea>
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-home"></i></span>
                                            </div>
                                            <div class="col-sm-5">
                                                <span style="color: red;">@error('alamat'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="input-group mb-3">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">No
                                                Telp/Handphone</label>
                                            <input type="text" name="numberPhone" class="form-control"
                                                placeholder="masukkan no handphone">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i
                                                        class="fas fa-phone-square"></i></span>
                                            </div>
                                            <div class="col-sm-5">
                                                <span
                                                    style="color: red;">@error('numberPhone'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="input-group mb-3">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                            <input type="email" name="email" class="form-control"
                                                placeholder="masukkan email">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                            </div>
                                            <div class="col-sm-5">
                                                <span style="color: red;">@error('email'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-header"></div>
                                    <div class="card-body">
                                        <div class="input-group mb-3">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Username</label>
                                            <input type="text" name="username" class="form-control"
                                                placeholder="masukkan username">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="far fa-user-circle"></i></span>
                                            </div>
                                            <div class="col-sm-5">
                                                <span
                                                    style="color: red;">@error('username'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="input-group mb-3">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
                                            <input type="password" name="password" class="form-control"
                                                placeholder="masukkan password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                            </div>
                                            <div class="col-sm-5">
                                                <span
                                                    style="color: red;">@error('password'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="input-group mb-3">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Ulangi
                                                Password</label>
                                            <input type="password" name="passwordRepeat" class="form-control"
                                                placeholder="masukkan password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                                            </div>
                                            <div class="col-sm-5">
                                                <span
                                                    style="color: red;">@error('passwordRepeat'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit Pendaftaran</button>
                                        <button onclick="return confirm('Are you sure?')" type="submit"
                                            class="btn btn-danger">Batal</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    @include('sweetalert::alert')
</body>

</html>
