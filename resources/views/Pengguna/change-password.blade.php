<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Update Password</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Update Password</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">

                                    </h5>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('simpan-data-change-password', $getData[0]->id) }}" method="post">
                                        {{ csrf_field() }}
                                        @if(Session::has('error'))
                                        <div class="alert alert-danger">
                                                {{ Session::get('error') }}
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Recent Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="recent_passswords" name="recent_passsword"
                                                    placeholder="masukkan password" >
                                                <span style="color: red;">@error('recent_passsword'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Change Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="change_passwords" name="change_password"
                                                    placeholder="masukkan password">
                                                <span style="color: red;">@error('change_password'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Confirmation Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="confirmation_passwords" name="confirmation_password"
                                                    placeholder="masukkan password">
                                                <span style="color: red;">@error('confirmation_password'){{ $message }}@enderror</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <a class="btn btn-outline-danger" href="{{ route('account-setting', auth()->user()->name) }}">Back</a>
                                            <button type="submit" class="btn btn-outline-primary">Edit Data</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-center">
                                        {{-- {!! $dtProduk->links() !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    {{-- @include('sweetalert::alert') --}}
    <script>
        document.querySelectorAll('input[type-currency="IDR"]').forEach((element) => {
            element.addEventListener('keyup', function(e) {
                let cursorPostion = this.selectionStart;
                let value = parseInt(this.value.replace(/[^,\d]/g, ''));
                let originalLenght = this.value.length;
                if (isNaN(value)) {
                this.value = "";
                } else {
                this.value = value.toLocaleString('id-ID', {
                    currency: 'IDR',
                    style: 'currency',
                    minimumFractionDigits: 0
                });
                cursorPostion = this.value.length - originalLenght + cursorPostion;
                this.setSelectionRange(cursorPostion, cursorPostion);
                }
            });
        });
    </script>

</body>

</html>
