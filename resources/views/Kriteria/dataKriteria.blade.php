<!DOCTYPE html>

<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('Template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('Template.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Kriteria</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Data Kriteria</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tabKriteria">Kriteria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('data-sub-kriteria') }}">Sub Kriteria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('data-kategori') }}">Detail Sub Kriteria</a>
                    </li>
                </ul>

            </div>
            <!-- /.content -->
            <div class="tab-content">
                <div class="tab-pane container active" id="tabKriteria">
                    <div class="card card-info card-outline">
                        <div class="card-header">
                            <div class="card-tools">
                                {{-- <a href="#" class="btn btn-success" data-toggle="modal"
                                    data-target="#modalCreate">Tambah
                                    Data <i class="fas fa-plus-square"></i></a> --}}
                                <a href="{{ route('create-kriteria') }}" class="btn btn-success">Tambah Data <i
                                        class="fas fa-plus-square"></i></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover">
                                <thead class="table table-bordered">
                                    <tr>
                                        <th>#</th>
                                        <th>Kriteria</th>
                                        <th>Ranking</th>
                                        <th>Bobot Roc</th>
                                    </tr>
                                </thead>
                                @foreach ($dtKriteria as $items => $item)
                                <tbody class="table table-bordered">
                                    <tr>
                                        <td>{{ $dtKriteria->firstItem() + $items }}</td>
                                        <td>{{ $item->nama_kriteria }} </td>
                                        <td>{{ $item->ranking }}</td>
                                        <td>{{ $item->bobot_roc }}</td>
                                        <td>
                                            <a href="{{ route('edit-kriteria', $item->id) }}"><i
                                                    class="fas fa-edit"></i></a> |
                                            <a onclick="return confirm('Are you sure?')"
                                                href="{{ route('delete-kriteria', $item->id) }}"><i
                                                    class="fas fa-trash-alt" style="color:red"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-center">
                                {!! $dtKriteria->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        @include('Template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    @include('Template.script')
    @include('sweetalert::alert')
    @include('kriteria.modal.createKriteria')
</body>

</html>
