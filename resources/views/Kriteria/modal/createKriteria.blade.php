<form action="{{ route('simpan-kriteria') }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal fade text-left" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Create New User') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama" name="nama"
                                placeholder="masukkan nama kriteria">
                            <span style="color: red;">@error('nama'){{ $message }}@enderror</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Ranking</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="ranking" name="ranking"
                                placeholder="masukkan ranking">
                            <span style="color: red;">@error('ranking'){{ $message }}@enderror</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Bobot</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="bobot" name="bobot"
                                placeholder="masukkan jumlah bobot">
                            <span style="color: red;">@error('bobot'){{ $message }}@enderror</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"
                            aria-label="Close">Back</button>
                        {{-- <a class="btn btn-outline-danger" href="{{ route('data-kategori') }}">Back</a> --}}
                        <button type="submit" class="btn btn-outline-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
