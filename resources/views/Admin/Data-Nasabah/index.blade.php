<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Data Nasabah</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Data Nasabah </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">

                                    </h5>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover">
                                        <thead class="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Lengkap</th>
                                                <th>No. Telepon</th>
                                                <th>Alamat</th>
                                                <th>Email</th>
                                                {{-- <th>Aksi</th> --}}
                                            </tr>
                                        </thead>
                                        @foreach ($customer as $items => $item)
                                        <tbody class="table table-bordered">
                                            <tr>
                                                <td>{{ $customer->firstItem() + $items }}</td>
                                                <td>{{ $item->fullname }} </td>
                                                <td>{{ $item->no_handphone }}</td>
                                                <td>{{ $item->alamat }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>
                                                    <a href="#" data-id="tol" data-toggle="modal"
                                                        data-target="#modal-default{{ $item->id }}"><i
                                                            class="fas fa-edit"></i>
                                                    </a>
                                                    <a onclick="return confirm('Are you sure?')"
                                                        href="{{ route('delete-data-nasabah', $item->id) }}">
                                                        <i class="fas fa-trash-alt" style="color:red"></i></a>
                                                    <div class="modal fade" id="modal-default{{ $item->id }}">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Edit Data Nasabah
                                                                    </h4>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <form
                                                                    action="{{ route('update-data-nasabah', $item->id) }}"
                                                                    method="post">
                                                                    {{ csrf_field() }}
                                                                    <div class="modal-body">
                                                                        <div class="form-group row">
                                                                            <label class="col-sm-3 col-form-label">Nama
                                                                                Lengkap</label>
                                                                            <div class="col-sm-8">
                                                                                <input class="form-control" type="text"
                                                                                    name="nama"
                                                                                    value="{{ $item->fullname }}" />
                                                                                <span
                                                                                    style="color: red;">@error('nama'){{ $message }}@enderror</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-sm-3 col-form-label">No.
                                                                                Telepon
                                                                            </label>
                                                                            <div class="col-sm-8">
                                                                                <input class="form-control" type="text"
                                                                                    name="no_telp"
                                                                                    value="{{ $item->no_handphone }}" />
                                                                                <span
                                                                                    style="color: red;">@error('no_telp'){{ $message }}@enderror</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label
                                                                                class="col-sm-3 col-form-label">Alamat
                                                                            </label>
                                                                            <div class="col-sm-8">
                                                                                <input class="form-control" type="text"
                                                                                    name="alamat"
                                                                                    value="{{ $item->alamat }}" />
                                                                                <span
                                                                                    style="color: red;">@error('alamat'){{ $message }}@enderror</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                            <label class="col-sm-3 col-form-label">Email
                                                                            </label>
                                                                            <div class="col-sm-8">
                                                                                <input class="form-control" type="text"
                                                                                    name="email"
                                                                                    value="{{ $item->email }}" />
                                                                                <span
                                                                                    style="color: red;">@error('email'){{ $message }}@enderror</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer justify-content-between">
                                                                        <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close</button>
                                                                        <button type="submit"
                                                                            class="btn btn-primary">Update</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-center">
                                        {!! $customer->links() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    @include('sweetalert::alert')

</body>

</html>
