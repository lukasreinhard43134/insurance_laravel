<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Hasil Pemilihan</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Hasil Pemilihan</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">

                                    </h5>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover">
                                        <thead class="table table-bordered">
                                            <tr>
                                                <th>No</th>
                                                <th>Tanggal Pemilihan</th>
                                                <th>Nama Lengkap</th>
                                                <th>No Telp</th>
                                                <th>Alamat</th>
                                                <th>Email</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody class="table table-bordered">
                                            @foreach ($getData as $items => $item)
                                            @php
                                                $i = DB::table('tbl_customer')->where('fullname',$item->customer)->first();
                                            @endphp
                                            <tr>
                                                <td>{{ $getData->firstItem() + $items }}</td>
                                                <td>{{ $item->created_at }}</td>
                                                <td>{{ $item->customer }}</td>
                                                <td>{{ $i->no_handphone }}</td>
                                                <td>{{ $i->alamat }}</td>
                                                <td>{{ $i->email }}</td>
                                                <td>
                                                    <a href="{{ route('detail-hasil', [$item->kode_pemilihan, $item->customer]) }}"><i
                                                            class="fas fa-edit"></i></a>| <a
                                                        onclick="return confirm('Are you sure?')" href="{{ route('delete-hasil', [$item->kode_pemilihan, $item->customer]) }}"><i
                                                            class="fas fa-trash-alt" style="color:red"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-center">
                                        {{-- {!! $dtProduk->links() !!} --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    @include('sweetalert::alert')
    <script>
        document.querySelectorAll('input[type-currency="IDR"]').forEach((element) => {
            element.addEventListener('keyup', function(e) {
                let cursorPostion = this.selectionStart;
                let value = parseInt(this.value.replace(/[^,\d]/g, ''));
                let originalLenght = this.value.length;
                if (isNaN(value)) {
                this.value = "";
                } else {
                this.value = value.toLocaleString('id-ID', {
                    currency: 'IDR',
                    style: 'currency',
                    minimumFractionDigits: 0
                });
                cursorPostion = this.value.length - originalLenght + cursorPostion;
                this.setSelectionRange(cursorPostion, cursorPostion);
                }
            });
        });
    </script>

</body>

</html>
