<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('Template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('Template.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{-- <h1 class="m-0">Edit </h1> --}}
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Edit Sub Kriteria</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="card card-info card-outline">
                    <div class="card-header">
                        <h3>Edit Sub Kriteria</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('update-sub-kriteria', $subkrite->id) }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Kriteria</label>
                                <div class="col-sm-10">
                                    <input type="text" id="kriteria" name="kriteria" class="form-control"
                                        placeholder="nama kriteria" disabled
                                        value="{{ $subkrite->kriteria->nama_kriteria }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Sub Kriteria</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="namas" name="nama" value="{{ $subkrite->nama }}"
                                        placeholder="masukkan nama" />
                                    <span style="color: red;">@error('nama'){{ $message }}@enderror</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Ranking</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="rankings" name="ranking" value="{{ $subkrite->ranking }}"
                                        placeholder="masukkan ranking" />
                                    <span style="color: red;">@error('ranking'){{ $message }}@enderror</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Bobot</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="bobot" name="bobot"
                                        placeholder="masukkan nilai bobot" value="{{ $subkrite->bobot }}" />
                                    <span style="color: red;">@error('bobot'){{ $message }}@enderror</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Tipe Input</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tipe_inputx" name="tipe_input"
                                        placeholder="masukkan nilai bobot" value="{{ $subkrite->tipe_input }}" />
                                    <span style="color: grey;">pilih salah satu: input, pilihan, input_currency</span>
                                    <span style="color: red;">@error('tipe_input'){{ $message }}@enderror</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <a class="btn btn-outline-danger" href="{{ route('data-sub-kriteria') }}">Back</a>
                                <button type="submit" class="btn btn-outline-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        @include('Template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    @include('Template.script')
</body>

</html>
