<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Web Application</title>

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome Icons -->
{{-- <link rel="stylesheet" href="../AdminLte/plugins/fontawesome-free/css/all.min.css"> --}}
<link rel="stylesheet" href="{{ asset('AdminLte/plugins/fontawesome-free/css/all.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="../../AdminLte/dist/css/adminlte.min.css">
{{-- <link rel="stylesheet" href="{{ asset('/AdminLte/plugins/fontawesome-free/css/all.min.css') }}"> --}}

<link rel="stylesheet" href="{{ asset('CSS/MyStyle.css') }}">
<link rel="icon" href="{{ asset('Gambar/logo-laravel.ico') }}" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
