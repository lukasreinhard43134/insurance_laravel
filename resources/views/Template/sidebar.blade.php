<aside class="main-sidebar sidebar-light-primary elevation-4 position-fixed">
    <div class="nav flex-column flex-nowrap vh-100 overflow-auto">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            {{-- <img style="max-width:220px;" src="{{ asset('Gambar/sidebar.jpg') }}" alt="AdminLTE Logo" class="brand-image"> --}}
            <img src="{{ asset('Gambar/logo-sidebar.png') }}" alt="AdminLTE Logo" class="brand-image img-rounded elevation-2">
            <span class="brand-text font-weight-light">Asuransi<b> Sinarmas</b></span>
        </a>
        {{-- <div class="alert alert-danger">
            {{ Session::get('isRole') }}
        </div> --}}
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu Admin-->
            @if ( Session::get('isRole')=="admin")
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{ route('beranda') }}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Home
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('data-produk') }}" class="nav-link">
                            <i class="nav-icon fas fa-box-open"></i>
                            <p>
                                Produk Asuransi
                            </p>
                        </a>
                    </li>

                    {{-- <li class="nav-item menu">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-list"></i>
                            <p>
                                Pengaturan Kategori
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @if (auth()->user()->role=="admin")
                            <li class="nav-item">
                                <a href="{{ route('data-kriteria') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Kriteria</p>
                    </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a href="{{ route('data-sub-kriteria') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Sub Kriteria</p>
                        </a>
                    </li>
                </ul>
                </li> --}}

                <li class="nav-item">
                    <a href="{{ route('data-kriteria') }}" class="nav-link">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Pengaturan Kriteria
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('data-bobot-gap') }}" class="nav-link">
                        <i class="nav-icon fas fa-sort-numeric-up"></i>
                        <p>
                            Bobot GAP
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('data-nilai-produk') }}" class="nav-link">
                        <i class="nav-icon far fa-calendar-check"></i>
                        <p>
                            Nilai Produk Asuransi
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('data-nasabah') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Data Nasabah
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('data-hasil-pemilihan-admin') }}" class="nav-link">
                        <i class="nav-icon fas fa-calculator"></i>
                        <p>
                            Data Pemilihan Asuransi
                        </p>
                    </a>
                </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu Admin-->
            @endif
            {{-- Sidebar Menu Customer --}}
            @if( Session::get('isRole')=="customer" || Session::get('isGuest')=="true" )
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <li class="nav-item">
                        <a href="{{ route('beranda') }}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Home
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('data-produk') }}" class="nav-link">
                            <i class="nav-icon fas fa-box-open"></i>
                            <p>
                                Produk Asuransi
                            </p>
                        </a>
                    </li>
                    @if(Session::get('isGuest') =="true" )
                    <li class="nav-item">
                        <a href="{{ route('register') }}" class="nav-link">
                            <i class="nav-icon far fa-file-alt"></i>
                            <p>
                                Pendaftaran Nasabah
                            </p>
                        </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a href="{{ route('data-pemilihan') }}" class="nav-link">
                            <i class="nav-icon far fa-hand-point-up"></i>
                            <p>
                                Pemilihan
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('data-hasil-pemilihan') }}" class="nav-link">
                            <i class="nav-icon far fa-calendar-check"></i>
                            <p>
                                Hasil Pemilihan
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            @endif

            {{-- /.Sidebar Menu Customer --}}
        </div>
        <!-- /.sidebar -->
    </div>
</aside>
