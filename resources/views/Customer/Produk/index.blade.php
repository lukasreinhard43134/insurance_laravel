<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Daftar Produk Asuransi</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="#">Product Asuransi</a></li>
                                <li class="breadcrumb-item active">Daftar Produk Asuransi </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">

                                    </h5>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover">
                                        <thead class="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                {{-- <th>Keterangan</th> --}}
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        @foreach ($dtProduk as $items => $item)
                                        <tbody class="table table-bordered">
                                            <tr>
                                                {{-- <td>{{ $loop->iteration }}</td> --}}
                                                <td>{{ $dtProduk->firstItem() + $items }}</td>
                                                <td>{{ $item->nama_produk }} </td>
                                                {{-- <td>{{ $item->keterangan_produk }}</td> --}}
                                                <td>
                                                    <a class="btn btn-outline-primary" href="#" data-id="tol"
                                                        data-toggle="modal"
                                                        data-target="#modal-default{{ $item->id_produk }}"><i
                                                            class="fas fa-eye"></i>
                                                        Detail</a>
                                                    <div class="modal fade" id="modal-default{{ $item->id_produk }}">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Detail Produk Asuransi
                                                                    </h4>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label
                                                                            class="col-sm-3 col-form-label">Nama</label>
                                                                        <input class="col-sm-8" type="text" disabled
                                                                            value="{{ $item->nama_produk }}" />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label
                                                                            class="col-sm-3 col-form-label">Keterangan</label>
                                                                        <input class="col-sm-8" type="text" disabled
                                                                            value="{{ $item->keterangan_produk }}" />
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close</button>
                                                                    {{-- <button type="button" class="btn btn-primary">Save
                                                                        changes</button> --}}
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-center">
                                        {!! $dtProduk->links() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    @include('sweetalert::alert')

</body>

</html>
