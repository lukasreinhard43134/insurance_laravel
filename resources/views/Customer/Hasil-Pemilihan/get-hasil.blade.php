<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Hasil Pemilihan</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Hasil Pemilihan </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="card card-grey card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill"
                                        href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home"
                                        aria-selected="true">Nilai Input</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill"
                                        href="#custom-tabs-one-profile" role="tab"
                                        aria-controls="custom-tabs-one-profile" aria-selected="false">Smarter</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill"
                                        href="#custom-tabs-one-messages" role="tab"
                                        aria-controls="custom-tabs-one-messages" aria-selected="false">Hasil
                                        Pemilihan</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel"
                                    aria-labelledby="custom-tabs-one-home-tab">
                                    @foreach ($getInputData as $item)
                                    <div class="form-group row">
                                        <label for="inputEmail3"
                                            class="col-sm-2 col-form-label">{{ $item->nama }}</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="nama" name="nama"
                                                value="{{ $item->nilai }}" disabled>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel"
                                    aria-labelledby="custom-tabs-one-profile-tab">
                                    <label for="nilai" style="color: red">Data Nilai Produk Asuransi</label>
                                    <table class="table table-hover">
                                        <thead class="table table-bordered">
                                            <tr>
                                                <th rowspan="3"
                                                    style="vertical-align: middle; background:rgb(197, 197, 197);">#
                                                </th>
                                                <th rowspan="3"
                                                    style="vertical-align: middle; background:rgb(197, 197, 197);">
                                                    Produk
                                                </th>
                                                <th colspan="8"
                                                    style="text-align: center; background:rgb(197, 197, 197);">Kriteria
                                                </th>
                                            </tr>
                                            <tr>
                                                @foreach ($getKriteria as $item)
                                                @php
                                                $i=App\Models\SubKriteria::all()->where('id_kriteria',$item->id);
                                                @endphp
                                                <th colspan="{{ count($i) }}"
                                                    style="text-align: center; background:rgb(197, 197, 197);">
                                                    {{ $item->nama_kriteria }}</th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                @foreach ($getKriteria as $item)
                                                @php
                                                $i=App\Models\SubKriteria::all()->where('id_kriteria',$item->id);
                                                @endphp
                                                @foreach ($i as $items)
                                                <th style="text-align: center; background:rgb(197, 197, 197);">
                                                    {{ $items->nama }}</th>
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        </thead>
                                        {{-- @foreach ($customer as $items => $item) --}}
                                        <tbody class="table table-bordered">
                                            @foreach ($getProduk as $items => $item)
                                            <tr>
                                                <th>{{ $getProduk->firstItem() + $items }}</th>
                                                <th>{{ $item->nama_produk }}</th>
                                                @php
                                                $i=App\Models\NilaiProduk::all()->where('nama_produk',$item->nama_produk);
                                                @endphp
                                                @foreach ($i as $items)
                                                @php
                                                $test = intval(preg_replace('/,.*|[^0-9]/', '', $items->nilai));
                                                $x=App\Models\Kategori::all()->where('sub_kriteria_nama',$items->nama);
                                                // foreach ($x as $value) {
                                                // echo($value->nama);
                                                // }
                                                @endphp
                                                <td>{{ ((int)$items->nilai) }}</td>

                                                @endforeach
                                            </tr>
                                            @endforeach
                                            {{-- <tr style="background: yellow;">
                                                <td>#</td>
                                                <th>{{ auth()->user()->name }}</th>
                                            @foreach ($profilNasabah as $item)
                                            <td>{{ $item->nilai }}</td>
                                            @endforeach
                                            </tr> --}}

                                            <tr style="background: yellow;">
                                                <td>#</td>
                                                <th>{{ auth()->user()->name }}</th>
                                                @foreach ($profilNasabah as $item)
                                                <td>{{ $item->nilai }}</td>
                                                @endforeach
                                            </tr>
                                        </tbody>
                                        {{-- <tfoot>
                                            <div class="d-flex justify-content-center">
                                                {!! $getProduk->links() !!}
                                            </div>
                                            <tr>
                                                <td>test</td>
                                            </tr>
                                        </tfoot> --}}
                                        {{-- @endforeach --}}
                                    </table>
                                    <label for="nilai" style="color: red">Nilai Utility</label>
                                    <table class="table table-hover">
                                        <thead class="table table-bordered">
                                            <tr>
                                                <th rowspan="3"
                                                    style="vertical-align: middle; background:rgb(197, 197, 197);">#
                                                </th>
                                                <th rowspan="3"
                                                    style="vertical-align: middle; background:rgb(197, 197, 197);">
                                                    Produk
                                                </th>
                                                <th colspan="8"
                                                    style="text-align: center; background:rgb(197, 197, 197);">Kriteria
                                                </th>
                                            </tr>
                                            <tr>
                                                @foreach ($getKriteria as $item)
                                                @php
                                                $i=App\Models\SubKriteria::all()->where('id_kriteria',$item->id);
                                                @endphp
                                                <th colspan="{{ count($i) }}"
                                                    style="text-align: center; background:rgb(197, 197, 197);">
                                                    {{ $item->nama_kriteria }}</th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                @foreach ($getKriteria as $item)
                                                @php
                                                $i=App\Models\SubKriteria::all()->where('id_kriteria',$item->id);
                                                @endphp
                                                @foreach ($i as $items)
                                                <th style="text-align: center; background:rgb(197, 197, 197);">
                                                    {{ $items->nama }}</th>
                                                @endforeach
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $nilaiUtility = [];
                                            @endphp
                                            @foreach ($getProduk as $items => $item)
                                            <tr>
                                                <th>{{ $getProduk->firstItem() + $items }}</th>
                                                <th>{{ $item->nama_produk }}</th>
                                                @php
                                                $i=App\Models\NilaiProduk::all()->where('nama_produk',$item->nama_produk);
                                                @endphp
                                                @foreach ($i as $items => $x)
                                                @php
                                                $j=App\Models\BobotGap::all()->where('selisih', $x->nilai - $arx[$items]);
                                                $bobotSubKriteria=App\Models\SubKriteria::all()->where('nama', $x->nama);
                                                $arrNilai = array();
                                                foreach ($bobotSubKriteria as $nilaiSubKriteria) {
                                                // echo($nilaiSubKriteria->bobot);
                                                array_push($arrNilai, $nilaiSubKriteria->bobot);
                                                }
                                                // print_r($arrNilai[0]);
                                                @endphp
                                                @foreach ($j as $bobot)
                                                @php
                                                // print("<pre>". print_r($x, true) ."</pre>");
                                                $obj = (object) [
                                                'nama_produk' => $item->nama_produk,
                                                'sub_kriteria' => $x->nama,
                                                'nilai' => $arrNilai[0] * $bobot->bobot
                                                ];
                                                array_push($nilaiUtility, $obj);
                                                @endphp
                                                {{-- <td>{{ $bobot->bobot}}</td> --}}
                                                {{-- <td>{{ $arrNilai[0]}}</td> --}}
                                                <td>{{ $arrNilai[0] * $bobot->bobot  }}</td>
                                                @endforeach
                                                {{-- @foreach ($bobotSubKriteria as $nilaiSubKriteria)
                                                <td>{{ $nilaiSubKriteria->bobot }}</td>
                                                @endforeach --}}
                                                {{-- <td>{{ $arrNilai[0] }}</td> --}}
                                                {{-- <td>{{ $x->nilai - $arx[$items] }}</td> --}}
                                                @endforeach
                                            </tr>
                                            @endforeach
                                            @php
                                            // print_r($nilaiUtility)
                                            $arr=[];
                                            // foreach ($nilaiUtility as $key => $element) {
                                            // $arr[$element->nama_produk][$key] = $element;
                                            // }

                                            foreach ($nilaiUtility as $value) {
                                                $arr[$value->nama_produk][] = $value;
                                            }
                                            @endphp

                                        </tbody>
                                    </table>
                                    <label for="nilai" style="color: red">Nilai Akhir</label>
                                    <table class="table table-hover">
                                        <thead class="table table-bordered">
                                            <tr>
                                                <th style="text-align: center; background:rgb(197, 197, 197);">No</th>
                                                <th style="text-align: center; background:rgb(197, 197, 197);">Produk</th>
                                                @foreach ($getKriteria as $item)
                                                @php
                                                $i=App\Models\SubKriteria::all()->where('id_kriteria',$item->id);
                                                @endphp
                                                <th style="text-align: center; background:rgb(197, 197, 197);">
                                                    {{ $item->nama_kriteria }}</th>
                                                @endforeach
                                                <th style="text-align: center; background:rgb(197, 197, 197);">Total
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="table table-bordered">
                                            @php
                                            $kas=[];
                                            foreach ($nilaiUtility as $value) {
                                                $key = $value->nama_produk;
                                                if(!array_key_exists($key,$kas)){
                                                    $kas[$key] = array(
                                                        'nama_produk'=> $value->nama_produk,
                                                        // 'sub_kriteria'=> $value->sub_kriteria,
                                                        'nilai'=> $value->nilai,
                                                    );
                                                }else {
                                                    $kas[$key]['nilai'] = $kas[$key]['nilai'] + $value->nilai;
                                                }

                                            }
                                            if(Session::get('isRole')=="customer"){
                                                foreach($kas as $data){
                                                    $getExistKodePemilihan=DB::table('hasil_pemilihan')->where('kode_pemilihan', 'LIKE', $kode_pemilihan)->where('customer', 'LIKE' ,auth()->user()->name)->get();
                                                    // print_r(count($getExistKodePemilihan));
                                                    if(count($getExistKodePemilihan) > 4){
                                                        // echo('datanya ada');
                                                    }else{
                                                        // echo('datanya tidak ada');
                                                        $insertData=App\Models\hasilPemilihan::create([
                                                            'nama_produk' => $data['nama_produk'],
                                                            'nilai' => $data['nilai'],
                                                            'customer' => auth()->user()->name,
                                                            'kode_pemilihan' => $kode_pemilihan,
                                                        ]);
                                                    }
                                                }
                                            }
                                            @endphp
                                            @foreach ($getProduk as $items => $item)
                                            @php
                                            $subKriteria = $arr[$item->nama_produk];
                                            foreach ($subKriteria as $valueSubKriteria) {
                                                $getKriterias = App\Models\SubKriteria::all()->where('nama', 'LIKE', $valueSubKriteria->sub_kriteria );
                                                foreach ($getKriterias as $key => $valueGetKriteriasss) {
                                                    if ($valueSubKriteria->sub_kriteria === $valueGetKriteriasss->nama) {
                                                        $valueSubKriteria->id_kriteria = $valueGetKriteriasss->id_kriteria;
                                                    }
                                                }
                                            }
                                            $kas2=[];
                                            foreach ($arr[$item->nama_produk] as $valueArr) {
                                                $key2 = $valueArr->id_kriteria;
                                                if(!array_key_exists($key2,$kas2)){
                                                    $kas2[$key2] = array(
                                                        'nama_produk'=>$item->nama_produk,
                                                        'nilai'=> $valueArr->nilai,
                                                    );
                                                }else {
                                                    $kas2[$key2]['nilai'] = $kas2[$key2]['nilai'] + $valueArr->nilai;
                                                }
                                                // array_push($arrSmarter, $kas2);
                                            }
                                            @endphp
                                            <tr>
                                                <th>{{ $getProduk->firstItem() + $items }}</th>
                                                <th>{{ $item->nama_produk }}</th>
                                                <td>{{ print_r($kas2[1]['nilai']) }}</td>
                                                <td>{{ print_r($kas2[2]['nilai']) }}</td>
                                                <td style="color:blue;">{{ print_r($kas[$item->nama_produk]['nilai']) }}</td>
                                            </tr>
                                            @endforeach
                                            @php
                                            @endphp
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel"
                                    aria-labelledby="custom-tabs-one-messages-tab">
                                    <label for="nilai" style="color: red">Hasil Smarter</label>
                                    <table class="table table-hover">
                                        <thead class="table table-bordered">
                                            <tr>
                                                {{-- <th>Rank</th> --}}
                                                <th>Produk</th>
                                                <th>Nilai</th>
                                            </tr>
                                        </thead>
                                        <tbody class="table table-bordered">
                                            @php
                                                $rank = [];
                                                foreach($kas as $datax => $itemKasx){
                                                    $rank[$datax] = $itemKasx['nilai'];
                                                }
                                                array_multisort($rank, SORT_DESC, $kas);
                                                // print_r($rank);
                                            @endphp
                                            @foreach ($kas as $t => $itemKas)
                                            <tr>
                                                {{-- {{-- <th>{{ count($kas) }}</th> --}}
                                                <td>{{ $t }}</td>
                                                <td>{{ $rank[$t] }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            @if ( Session::get('isRole')=="customer")
                            @php
                                // foreach($kas as $data){
                                //     $getExistKodePemilihan=DB::table('hasil_pemilihan')->where('kode_pemilihan', 'LIKE', $kode_pemilihan)->where('customer', 'LIKE' ,auth()->user()->name)->get();
                                //     // print_r(count($getExistKodePemilihan));
                                //     if(count($getExistKodePemilihan) > 0){
                                //         // echo('datanya ada');
                                //     }else{
                                //         // echo('datanya tidak ada');
                                //         $insertData=App\Models\hasilPemilihan::create([
                                //             'nama_produk' => $data['nama_produk'],
                                //             'nilai' => $data['nilai'],
                                //             'customer' => auth()->user()->name,
                                //             'kode_pemilihan' => $kode_pemilihan,
                                //         ]);
                                //     }
                                // }
                            @endphp
                            <a class="btn btn-outline-danger" href="{{ route('data-hasil-pemilihan') }}">Back</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    @include('sweetalert::alert')

</body>

</html>
