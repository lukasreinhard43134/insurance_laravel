<!DOCTYPE html>
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('Template.navbar')
        @include('Template.sidebar')

        <!--content page-->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Pemilihan Produk Asuransi</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Pemilihan Produk Asuransi </li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">
                                        Silakan isi data berikut dengan benar
                                    </h5>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('simpan-pemilihan') }}" method="post">
                                        {{ csrf_field() }}
                                        @foreach ($subKriteria as $datas )
                                        @if($datas->tipe_input == 'input')
                                        <div class="form-group row">
                                            <label for="inputEmail3"
                                                class="col-sm-2 col-form-label">{{ $datas->nama }}</label>
                                            <div class="col-sm-10">
                                                <input type="number"
                                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                    class="form-control" id="{{ $datas->nama }}" maxLength="2"
                                                    name="{{ $datas->nama }}"
                                                    placeholder="masukkan jumlah {{ $datas->nama }}" required>
                                                <span
                                                    style="color: red;">@error('{{ $datas->nama }}'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        @elseif ($datas->tipe_input == 'input_currency')
                                        <div class="form-group row">
                                            <label for="inputEmail3"
                                                class="col-sm-2 col-form-label">{{ $datas->nama }}</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control input-currency"
                                                    id="{{ $datas->nama }}" name="{{ $datas->nama }}" placeholder="Rp"
                                                    NumberOnly="true" type-currency="IDR" />
                                                <span
                                                    style="color: red;">@error('{{ $datas->nama }}'){{ $message }}@enderror</span>
                                            </div>
                                        </div>
                                        @elseif ($datas->tipe_input == 'pilihan')
                                        <div class="form-group row">
                                            <label for="inputEmail3"
                                                class="col-sm-2 col-form-label">{{ $datas->nama }}</label>
                                            <div class="col-sm-10">
                                                {{-- <input type="text" class="form-control" id="nama" name="nama"
                                                    placeholder="masukkan nama kriteria"> --}}
                                                <select class="form-control" name="{{ $datas->nama }}"
                                                    id="{{ $datas->nama }}">
                                                    <option>-- pilih salah satu --</option>
                                                    @php
                                                    $i=App\Models\Kategori::all()->where('sub_kriteria_nama',$datas->nama);
                                                    @endphp
                                                    @foreach ($i as $datass)
                                                    <option>{{ $datass->nama }}</option>
                                                    @endforeach
                                                </select>
                                                {{-- <span style="color: red;">@error('subKriteria'){{ $message }}@enderror</span>
                                                --}}
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        <div class="form-group">
                                            <a class="btn btn-outline-danger"
                                                href="{{ route('data-kriteria') }}">Back</a>
                                            <button type="submit" class="btn btn-outline-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-footer">
                                    {{-- <div class="d-flex justify-content-center">
                                        {!! $dtProduk->links() !!}
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-page -->

    @include('Template.footer')
    </div>
    @include('Template.script')
    @include('sweetalert::alert')
    <script>
        document.querySelectorAll('input[type-currency="IDR"]').forEach((element) => {
            element.addEventListener('keyup', function(e) {
                let cursorPostion = this.selectionStart;
                let value = parseInt(this.value.replace(/[^,\d]/g, ''));
                let originalLenght = this.value.length;
                if (isNaN(value)) {
                this.value = "";
                } else {
                this.value = value.toLocaleString('id-ID', {
                    currency: 'IDR',
                    style: 'currency',
                    minimumFractionDigits: 0
                });
                cursorPostion = this.value.length - originalLenght + cursorPostion;
                this.setSelectionRange(cursorPostion, cursorPostion);
                }
            });
        });
    </script>

</body>

</html>
