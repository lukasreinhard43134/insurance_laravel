<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('Template.head')
    <style>
    .content-wrapper {
            background-image: url('{{ asset('Gambar/background.jpg')}}');
            background-repeat: no-repeat;
            background-position: center;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('Template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('Template.sidebar')

        {{-- style="background-image: url('{{ asset('Gambar/logo-sidebar.png')}}')" --}}

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper sidebar-light-primary">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <h4 style="font-family: Lucida Console; text-align=center;"><b>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Selamat datang di Aplikasi Pemilihan Produk Asuransi</b></h4>

                        <div class="col-sm-8">
                            {{-- <img src="{{ asset('Gambar/sinarms.jpg') }}" alt="AdminLTE Background"  class="img-fluid" style="margin-bottom: 100px;"> --}}
                            {{-- &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp; Selamat datang di Aplikasi Pemilihan Produk Asuransi --}}
                            <h4 class="m-0">
                                {{-- &nbsp;<br><br><br><br><br><br><br><br><br><br><br><br><br><br> --}}
                                {{-- <div style="background-image: url('{{ asset('Gambar/logo-sidebar.png')}}'); background-repeat: no-repeat;"></div> --}}

                                {{-- style="background-image: url('{{ asset('Gambar/sinarms.jpg')}}'); background-repeat: no-repeat;" --}}
                            </h4>
                        </div><!-- /.col -->
                        <div class="col-sm-4">
                            <ol class="breadcrumb float-sm-right">
                                {{-- <li class="breadcrumb-item"><a href="#">Home</a></li> --}}
                                {{-- <li class="breadcrumb-item active">Starter Page</li> --}}
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            @if (Session::get('isGuest')=="true")
            <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card card-secondary card-outline">
                                    <div class="card-header">
                                        <h5 class="m-0">Login Nasabah</h5>
                                    </div>
                                    <div class="card-body">
                                        <form action="{{route('postLogin')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="input-group mb-3">
                                                <input type="email" class="form-control" name="email" placeholder="Email">
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <span class="fas fa-envelope"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span style="color: red;">@error('email'){{ $message }}@enderror</span>
                                            <div class="input-group mb-3">
                                                <input type="password" class="form-control" name="password" placeholder="Password">
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <span class="fas fa-lock"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <span style="color: red;">@error('password'){{ $message }}@enderror</span>
                                            <div class="row">
                                                {{-- <div class="col-4">
                                                    <div class="icheck-primary">
                                                        <input type="checkbox" id="remember">
                                                        <label for="remember">
                                                            Remember Me
                                                        </label>
                                                    </div>
                                                </div> --}}
                                                <!-- /.col -->
                                                <div class="col-6">
                                                    <button type="submit" class="btn btn-outline-primary">Sign In</button>
                                                    <a class="btn btn-outline-warning" href="{{ route('register') }}">Register</a>
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.card -->
                            </div>
                            <!-- /.col-md-6 -->
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="m-0">Informasi</h5>
                                    </div>
                                    <div class="card-body">
                                        {{-- <h6 class="card-title">Special title treatment</h6> --}}
                                        <p class="card-text">
                                        Untuk dapat masuk ke aplikasi, silahkan login dahulu pada form disamping
                                        , jika anda belum terdaftar anda bisa melakukan register terlebih dahulu
                                        </p>
                                        {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-6 -->
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
            @endif
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        @include('Template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    @include('Template.script')
</body>

</html>
