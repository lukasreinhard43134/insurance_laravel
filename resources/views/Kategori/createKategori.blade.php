<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('Template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('Template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('Template.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{-- <h1 class="m-0">Kategori</h1> --}}
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('data-kategori') }}">Detail Sub
                                        Kriteria</a></li>
                                <li class="breadcrumb-item active">Create Detail Sub Kriteria</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="card card-info card-outline">
                    <div class="card-header">
                        <h3>Create Detail Sub Kriteria</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('simpan-kategori') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Sub Kriteria</label>
                                <div class="col-sm-10">
                                    {{-- <input type="text" class="form-control" id="nama" name="nama"
                                        placeholder="masukkan nama kriteria"> --}}
                                    <select class="form-control" name="subKriteria" id="pekerjaan"
                                        onchange="myFunction()">
                                        <option>-- pilih salah satu --</option>
                                        @foreach ($getSubKriteria as $datas)
                                        <option value="{{ $datas->nama }}">{{ $datas->nama }}</option>
                                        @endforeach
                                    </select>
                                    {{-- <span style="color: red;">@error('subKriteria'){{ $message }}@enderror</span>
                                    --}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama" name="nama"
                                        placeholder="masukkan nama">
                                    <span style="color: red;">@error('nama'){{ $message }}@enderror</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Nilai</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nilai" name="nilai"
                                        placeholder="masukkan nilai">
                                    <span style="color: red;">@error('nilai'){{ $message }}@enderror</span>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label id="nilaiAwal" for="inputEmail3" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <p id="demo"></p>
                                    <span style="color: red;">@error('nilai'){{ $message }}@enderror</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label id="nilaiAkhir" for="inputEmail3" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                        <p id="inputAkhir"></p>
                        <span style="color: red;">@error('nilai'){{ $message }}@enderror</span>
                    </div>
                </div> --}}
                <p id="demo"></p>
                {{-- <input id="demo" /> --}}
                <div class="form-group">
                    <a class="btn btn-outline-danger" href="{{ route('data-kriteria') }}">Back</a>
                    <button type="submit" class="btn btn-outline-primary">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    @include('Template.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    @include('Template.script')
    {{-- <script>
        function myFunction() {
          var x = document.getElementById("pekerjaan").value;
          var awal = 'Nilai Awal';
          var inputAwal = '<input type="text" class="form-control" name="nilai_awal">';
          var akhir = 'Nilai Akhir';
          var inputAkhir = '<input type="text" class="form-control" name="nilai_akhir">';
          if(x == 'pendapatan'){
            //   document.getElementById("demo").innerHTML = "You selected: " + x;
            document.getElementById("demo").innerHTML = inputAwal ;
            document.getElementById("inputAkhir").innerHTML = inputAkhir ;
            document.getElementById("nilaiAwal").innerHTML = awal;
            document.getElementById("nilaiAkhir").innerHTML = akhir;
          }
        }
    </script> --}}
</body>

</html>
