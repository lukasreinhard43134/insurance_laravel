<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Session\Session;

use Illuminate\Http\Request;
use Symfony\Contracts\Service\Attribute\Required;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'fullname' => 'required',
            'alamat' => 'required',
            'numberPhone' => 'required|digits:12',
            'email' => 'required|email|unique:users,email',
            'username' => 'required',
            'password' => 'required',
            'passwordRepeat' => 'required|same:password'
        ]);

        Customer::create([
            'fullname' => $request->fullname,
            'alamat' => $request->alamat,
            'no_handphone' => $request->numberPhone,
            'email'=> $request->email,
            // 'role' => 'customer',
            // 'password' => bcrypt($request->password),
        ]);

        $user = new User;
        $user->name = $request->fullname;
        $user->role = 'customer';
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(60);
        $user->save();


        alert()->success('Data Created Successfully','Silakan log in terlebih dahulu.');
        return back()->with('success', 'User Created Successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function accountSetting($id){
        // dd($id);
        $getData = DB::table('users')->where('name', 'LIKE', $id)->paginate(100);
        return view('Pengguna.account-setting', compact('getData'));
    }

    public function getDetailAccount($id){
        if(auth()->user()->role === 'admin'){
            $getData = DB::table('users')->where('name', 'LIKE', $id)->get();
        }elseif (auth()->user()->role === 'customer') {
            $getData = DB::table('users')->join('tbl_customer', 'users.name', '=', 'tbl_customer.fullname')->where('tbl_customer.fullname', 'LIKE', $id)->get();
            // $getNilai = DB::table('kategori')->join('sub_kriteria', 'kategori.sub_kriteria_nama', '=', 'sub_kriteria.nama')->where('kategori.sub_kriteria_nama', 'LIKE', $value->nama)->where('sub_kriteria.tipe_input', 'NOT LIKE' ,'pilihan')->get();
        }
        // dd($getData[0]);
        return view('Pengguna.detail-account-setting', compact('getData'));
    }

    public function updateAccount(Request $request, $id){
        // dd($request->all());

        // $users = DB::table('users')->join('tbl_customer', 'users.name', '=', 'tbl_customer.fullname')->where('tbl_customer.fullname', 'LIKE', $id)->get();
        $customer = Customer::findorfail($id);
        $user = User::findorfail($id);
        $request->validate([
            'fullname' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required|digits:12',
        ]);
        $user->update([
            'name'=> $request->fullname,
        ]);
        $customer->update([
            'fullname'=> $request->fullname,
            'alamat' => $request->alamat,
            'no_handphone' => $request->no_hp,
        ]);
        // return back()->with('info', 'Data Updated Successfully!');
        // return redirect('login')->with('toast_success', 'Data Updated Successfully!');
        return redirect('login');
    }

    public function changePassUser($id){
        $getData = DB::table('users')->where('name', 'LIKE', $id)->paginate(100);
        return view('Pengguna.change-password', compact('getData'));
    }

    public function simpanPassUser(Request $request, $id){
        // dd($id);
        $getData = DB::table('users')->where('id', 'LIKE', $id)->paginate(100);
        $getPass = $getData[0]->password;

        $user = User::findOrFail($id);
        $request->validate([
            'recent_passsword' => 'required',
            'change_password' => 'required|max:25|different:recent_passsword',
            'confirmation_password' => 'required|same:change_password'
        ]);

        if (Hash::check($request->recent_passsword, $getPass)) {
            $user->fill([
             'password' => Hash::make($request->confirmation_password)
             ])->save();
            
            $request->session()->flash('success', 'Password changed');
            return view('Pengguna.account-setting', compact('getData'));

         } else {
            $request->session()->flash('error', 'Password does not match');
            return back();
         }
    }

    public function deleteAccount($id){
        $getData = DB::table('users')->where('name', 'LIKE', $id)->paginate(100);
        // dd($getData[0]);
        $getData->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }
}
