<?php

namespace App\Http\Controllers;

use App\Models\hasilPemilihan;
use Illuminate\Support\Facades\Session;

use App\Models\Kriteria;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $getRole = Session::get('isRole');
        $dtProduk = Produk::orderBy('created_at', 'desc')->paginate(5);

        if($getRole == 'admin'){
            return view('Produk.dataProduk', compact('dtProduk'));
        }else if($getRole == 'customer'){
            return view('Customer.Produk.index', compact('dtProduk'));
        }else{
            abort(403);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Produk.createProduk');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'produk' => 'required',
            'keterangan' => 'required'
        ]);
        Produk::create([
            'nama_produk' => $request->produk,
            'keterangan_produk' => $request->keterangan,
        ]);

        return redirect('data-produk')->with('toast_success', 'Data Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prod = Produk::findorfail($id);
        return view('Produk.editProduk', compact('prod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prod = Produk::findorfail($id);
        $hasilPemilihan = DB::table('hasil_pemilihan')->where('nama_produk', 'LIKE', $prod->nama_produk);
        $getNilaiProduk = DB::table('nilai_produk')->where('nama_produk', $prod->nama_produk);

        $request->validate([
            'produk' => 'required',
            'keterangan' => 'required'
        ]);
        $prod->update([
            'nama_produk' => $request->produk,
            'keterangan_produk' => $request->keterangan,
        ]);
        $hasilPemilihan->update([
                'nama_produk' => $request->produk
        ]);
        $getNilaiProduk->update([
            'nama_produk' => $request->produk
        ]);

        toast('Data Update Successfully','toast_success')->autoClose(3000);
        return redirect('data-produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prod = Produk::findorfail($id);
        $prod->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }

    // public function test()
    // {
    //     $getBobotKriteria = Kriteria::find(2);
    //     dd($getBobotKriteria->bobot_roc);
    // }

    public function indexCustomer()
    {
        $customer = Session::get('isRole');
        if($customer == 'customer'){
            $getData = Produk::orderBy('created_at', 'desc')->paginate(5);
            return view('Customer.Produk.index', compact('getData'));
            // abort(403);
        }
    }
}
