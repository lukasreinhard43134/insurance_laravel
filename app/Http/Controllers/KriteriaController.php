<?php

namespace App\Http\Controllers;

use App\Models\Kriteria;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;


class KriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtKriteria = Kriteria::orderBy('created_at', 'desc')->paginate('5');
        return view('Kriteria.dataKriteria', compact('dtKriteria'));
        // return view('Kriteria.dataKriteria');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Kriteria.createKriteria');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get total ranking
        // $countKriteria = Kriteria::Test();
        // print_r($countKriteria);

        $input = $request->ranking;
        $awal = array(0.75,0.25);
        $arr = [];
        $arr2 = [];

        // looping sebanyak inputan ranking
        for($x=1; $x<=$input; $x++){
            // print(1/$x);
            // $dt1 = 1/$x;
            array_push($arr, 1/$x);
            // print_r($arr);
        }

        // looping hasil array 1
        foreach ($arr as $value) {
            // print_r($value);
            array_push($arr2,$value/$input);
        }
        // print_r($arr2);


        // $i = count($arr2);
        // $i = 3;
        // $bawah = array(1,0.5);
        // while( $i > 0 ){
        //     // print_r($d_jenis);
        //     $i--;
        //     for($x=1; $x<=2; $x++){
        //         // print($i);
        //     }

        //         // print_r($bawah[$i]);
        //     print($i);

        // }



        // $result = array_splice($arr2, $input-1);
        // // $awal = $awal[0] - $result[0];


        // $test = [];
        // foreach ($awal as $values) {
        //     // print_r($values);
        //     if($awal[$values] == $values){
        //         // $test = $awal[$values] -
        //     }
        // }
        // print_r($awal);
        // print_r($result);




        // $inputs = array("a", "b", "c", "d", "e");
        // $outputs = array_slice($inputs, 3);
        // print_r($outputs);


        // dd($request->all());
        // $getBobotKriteria = Kriteria::all();
        // dd($getBobotKriteria[0]->bobot_roc);

        // $request->validate([
        //     'nama' => 'required',
        //     'ranking' => 'required|numeric',
        //     'bobot' => 'required|numeric'
        // ]);
        // Kriteria::create([
        //     'nama_kriteria' => $request->nama,
        //     'ranking' => $request->ranking,
        //     'bobot_roc' => $request->bobot,
        // ]);
        // return redirect('data-kriteria')->with('success', 'Data Created Successfully!');
        // return redirect('data-kategori')->with('toast_success', 'Data Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $krite = Kriteria::findorfail($id);
        return view('Kriteria.editKriteria', compact('krite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $krite = Kriteria::findorfail($id);
        // $prod->update($request->all());
        $krite->update([
            'nama_kriteria' => $request->nama,
            'ranking' => $request->ranking,
            'bobot_roc' => $request->bobot,
        ]);
        return redirect('data-kriteria')->with('toast_success', 'Data Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $krite = Kriteria::findorfail($id);
        $krite->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }
}
