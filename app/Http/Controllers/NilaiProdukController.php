<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Kriteria;
use Illuminate\Http\Request;

use App\Models\Produk;
use App\Models\NilaiProduk;
use App\Models\SubKriteria;
use Illuminate\Support\Facades\DB;


use Ramsey\Uuid\Rfc4122\NilUuid;

class NilaiProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $getProduk = Produk::all();
        // $dt = array();
        // foreach ($getProduk as $value) {
        //     foreach()
        //     $dt->$value->nama =
        // }



        // NilaiProduk::truncate();


        $dtNilai = NilaiProduk::all(); //get('pekerjaan');
        $subKriteria = SubKriteria::all();
        // $dtProduk = DB::table('nilai_produk')->join('kategori', 'nilai_produk.pekerjaan', '=', 'kategori.nilai')->get();
        // $getProduk = Produk::all();
        $getProduk = Produk::orderBy('created_at', 'desc')->paginate(5);

        // //send data
        $pekerjaan = Kategori::all()->where('sub_kriteria_nama', 'LIKE', 'pekerjaan');
        $usia = Kategori::all()->where('sub_kriteria_nama', 'LIKE', 'usia');
        $pendapatan = Kategori::all()->where('sub_kriteria_nama', 'LIKE', 'pendapatan');
        $jumlah_anak = Kategori::all()->where('sub_kriteria_nama', 'LIKE', 'jumlah anak');
        $status_pernikahan = Kategori::all()->where('sub_kriteria_nama', 'LIKE', 'status pernikahan');

        return view('NilaiProduk.dataNilai', compact('dtNilai', 'getProduk', 'subKriteria', 'pekerjaan', 'usia', 'pendapatan', 'status_pernikahan', 'jumlah_anak'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        DB::table('nilai_produk')->where('nama_produk', 'LIKE', $request->produk)->delete();

        $getSubKriteria = SubKriteria::all();
        foreach ($getSubKriteria as $value) {
            $z= $value->nama;
            NilaiProduk::create([
                'nama_produk'=>$request->produk,
                'nama' => $z,
                'nilai' => $request->$z,
            ]);
        }

        // NilaiProduk::truncate();
        // $getNilai = NilaiProduk::all();
        // // dd($request->all());

        // $nilai_produk_detail = [];
        // $produk = $request->all();
        // for ($i=0; $i < count($request->nama); $i++) {
        //     $nilai_produk_detail[]=[
        //         // 'id'=>
        //         'nama'=> $produk['nama'][$i],
        //         'pekerjaan' => $produk['pekerjaan'][$i],
        //         'usia' => $produk['usia'][$i],
        //         'pendapatan' => $produk['pendapatan'][$i],
        //         'status_pernikahan' => $produk['status_pernikahan'][$i],
        //         'jumlah_anak' => $produk['jumlah_anak'][$i]
        //     ];
        // }
        // NilaiProduk::insert($nilai_produk_detail);

        toast('Data Update Successfully','success')->autoClose(3000);
        return redirect('data-nilai-produk'); //->with('toast_question', 'Data Updated Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
