<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::orderBy('created_at', 'desc')->paginate(5);
        return view('Admin.Data-Nasabah.index', compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findorfail($id);
        $user = User::findorfail($id);
        // $getDataUser = DB::table('users')->where('name', 'LIKE', $request->nama)->get();
        // $user = User::findorfail($getDataUser[0]->id);
        // dd($user);
        $request->validate([
            'nama' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
            'email' => 'required|email',
        ]);
        $customer->update([
            'fullname' => $request->nama,
            'alamat' => $request->alamat,
            'no_handphone' => $request->no_telp,
            'email' => $request->email
        ]);
        $user->update([
            'name' => $request->nama,
            'email' => $request->email
        ]);

        toast('Data Updated Successfully','success')->autoClose(3000);
        return redirect('data-nasabah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findorfail($id);
        $user = User::findorfail($id);
        $customer->delete();
        $user->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }
}
