<?php

namespace App\Http\Controllers;

use App\Models\hasilPemilihan;
use App\Models\Kategori;
use App\Models\Kriteria;
use App\Models\NilaiProduk;
use App\Models\PemilihanIndex;
use App\Models\Produk;
use App\Models\SubKriteria;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Undefined;

class HasilPemilihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDetail()
    {
        //get profil nasabah
        $profilNasabah = [];
        $getInputData = DB::table('tbl_pemilihan')->join('sub_kriteria', 'tbl_pemilihan.nama', '=', 'sub_kriteria.nama')->where('tbl_pemilihan.customer', 'LIKE', auth()->user()->name)->get();

        foreach ($getInputData as $value) {
            if(preg_match('~[0-9]+~', $value->nilai)){
                $value->nilai = intval(preg_replace('/,.*|[^0-9]/', '', $value->nilai));
            }else{
                $value->nilai = $value->nilai;
            }
            // print_r($value->nilai. '<br>');
            $getNilai = DB::table('kategori')->join('sub_kriteria', 'kategori.sub_kriteria_nama', '=', 'sub_kriteria.nama')->where('kategori.sub_kriteria_nama', 'LIKE', $value->nama)->where('sub_kriteria.tipe_input', 'NOT LIKE' ,'pilihan')->get();
            $getPilihan = DB::table('kategori')->join('sub_kriteria', 'kategori.sub_kriteria_nama', '=', 'sub_kriteria.nama')->where('kategori.sub_kriteria_nama', 'LIKE', $value->nama)->where('sub_kriteria.tipe_input', 'LIKE' ,'pilihan')->select('kategori.nama as nama_kategori', 'sub_kriteria.nama as sub_nama', 'kategori.nilai as kategori_nilai')->get();
            // dd($getNilai);
            if($getNilai){
                foreach ($getNilai as $key ) {
                    if ($value->nilai >= ((int)$key->nilai_awal) && $value->nilai <= ((int)$key->nilai_akhir)) {
                        $obj = (object) [
                            'nama_produk' => auth()->user()->name,
                            'sub_kriteria' => $key->nama,
                            'nilai' => (int)$key->nilai
                        ];
                        array_push($profilNasabah, $obj);
                    }
                }
            }
            if($getPilihan){
                foreach ($getPilihan as $keys ) {
                    if (str_contains($value->nilai, $keys->nama_kategori)) {

                        $obj = (object) [
                            'nama_produk' => auth()->user()->name,
                            'sub_kriteria' => $keys->sub_nama,
                            'nilai' => (int)$keys->kategori_nilai
                        ];
                        array_push($profilNasabah, $obj);
                    }
                }
            }
        }
        //end

        $getKriteria = Kriteria::all();
        $getProduk = Produk::orderBy('created_at', 'desc')->paginate(100);

        //looping nilai nasabah sebanyak jumlah produk
        $getProduks = Produk::all();
        $getSub = SubKriteria::all();
        $arx = array();
        foreach($getProduks as $i => $values){
            foreach($getSub as $j => $valusss){
                $t = $profilNasabah[$j]->nilai;
                array_push($arx, $t);
            }
        }
        //-end

        // dd($getProduk);
        return view('Customer.Hasil-Pemilihan.detail', compact('getInputData', 'getKriteria', 'getProduk', 'profilNasabah','arx'));
    }

    public function index(){
        // get pemilihan
        // $getData = PemilihanIndex::orderBy('created_at', 'desc')->paginate(100);
        $getData = DB::table('pemilihan_index')->where('customer', 'LIKE', auth()->user()->name)->paginate(100);

        return view('Customer.Hasil-Pemilihan.index', compact('getData' ));
    }

    public function indexSubmit(){
        // return view('Customer.Hasil-Pemilihan.index', compact('getData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
