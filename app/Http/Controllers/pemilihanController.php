<?php

namespace App\Http\Controllers;

use App\Models\hasilPemilihan;
use App\Models\Kategori;
use App\Models\Pemilihan;
use App\Models\PemilihanIndex;
use App\Models\SubKriteria;
use App\Models\Kriteria;
use App\Models\Produk;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mockery\Undefined;

use function PHPUnit\Framework\isNan;
use function PHPUnit\Framework\isNull;

class pemilihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subKriteria = SubKriteria::all();
        $subKriteriaDetail = SubKriteria::all();
        return view('Customer.Pemilihan.index', compact('subKriteria','subKriteriaDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $getSubKriteria = SubKriteria::all();
        $getCode = PemilihanIndex::all()->where('customer', 'LIKE', auth()->user()->name);
        // dd($getCode);

        if (count($getCode) <= 0) {
            $kode_pemilihan = $this->generateCode('smarter_0');
        }else{
            foreach($getCode as $valueCode){
                $x = $valueCode->kode_pemilihan;
                $kode_pemilihan = $this->generateCode($x);
            }
        }

        // print_r($kode_pemilihan);

        PemilihanIndex::create([
            'customer'=> auth()->user()->name,
            'kode_pemilihan' => $kode_pemilihan,
        ]);
        foreach ($getSubKriteria as $value) {
            $z= $value->nama;
            // var_dump($z);
            // dd($request->$z);
            // print_r($request->only($z));
            Pemilihan::create([
                'kode_pemilihan' => $kode_pemilihan,
                'nama' => $z,
                'nilai' => $request->$z,
                'customer'=> auth()->user()->name
            ]);
        }
        // $pendapatan = intval(preg_replace('/,.*|[^0-9]/', '', $request->pendapatan));
        toast('Data Created Successfully','success')->autoClose(3000);
        $smarter = $this->smarter($kode_pemilihan,auth()->user()->name);
        return($smarter);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateCode($data){
        $getFirstString = substr($data, 0, 8);
        $getDigit = substr($data, 8);
        $addOne = $getDigit + 1;
        $kode_pemilihan = $getFirstString . $addOne;
        return($kode_pemilihan);
    }

    public function DataHasilPemilihanAdmin(){
        $getData = DB::table('pemilihan_index')->paginate(100);
        return view('Admin.PemilihanAsuransi.DataPemilihanAsuransi', compact('getData' ));
    }

    public function getDetailHasil($id, $customer){
        // dd($id, $customer);
        $smarter = $this->smarter($id, $customer);
        return($smarter);
    }

    public function deleteHasil($id, $customer)
    {
        // $index = PemilihanIndex::findorfail($id);
        $index = DB::table('pemilihan_index')->where('kode_pemilihan', 'LIKE', $id)->where('customer', 'LIKE', $customer);
        $pemilihan = DB::table('tbl_pemilihan')->where('kode_pemilihan', 'LIKE', $id)->where('customer', 'LIKE', $customer);
        $hasilPemilihan = DB::table('hasil_pemilihan')->where('kode_pemilihan', 'LIKE', $id)->where('customer', 'LIKE', $customer);
        // dd($hasilPemilihan);
        $index->delete();
        $pemilihan->delete();
        $hasilPemilihan->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }

    public function smarter($data, $customer){
        //getData
        $kode_pemilihan = $data;
        // $getInputData = Pemilihan::all()->where('kode_pemilihan', 'LIKE', $data)->where('customer', 'LIKE', auth()->user()->name)->get();
        $getInputData = DB::table('tbl_pemilihan')->where('kode_pemilihan', 'LIKE', $data)->where('customer', 'LIKE', $customer)->get();
        // dd($getInputData);
        $profilNasabah = [];
        foreach ($getInputData as $value) {
            if(preg_match('~[0-9]+~', $value->nilai)){
                $value->nilai = intval(preg_replace('/,.*|[^0-9]/', '', $value->nilai));
            }else{
                $value->nilai = $value->nilai;
            }
            // print_r($value->nilai. '<br>');
            $getNilai = DB::table('kategori')->join('sub_kriteria', 'kategori.sub_kriteria_nama', '=', 'sub_kriteria.nama')->where('kategori.sub_kriteria_nama', 'LIKE', $value->nama)->where('sub_kriteria.tipe_input', 'NOT LIKE' ,'pilihan')->get();
            $getPilihan = DB::table('kategori')->join('sub_kriteria', 'kategori.sub_kriteria_nama', '=', 'sub_kriteria.nama')->where('kategori.sub_kriteria_nama', 'LIKE', $value->nama)->where('sub_kriteria.tipe_input', 'LIKE' ,'pilihan')->select('kategori.nama as nama_kategori', 'sub_kriteria.nama as sub_nama', 'kategori.nilai as kategori_nilai')->get();
            // print("<pre>". print_r($getNilai, true) ."</pre>");
            // dd($getPilihan);
            if($getNilai){
                foreach ($getNilai as $key ) {
                    if ($value->nilai >= ((int)$key->nilai_awal) && $value->nilai <= ((int)$key->nilai_akhir)) {
                        $obj = (object) [
                            'nama_produk' => auth()->user()->name,
                            'sub_kriteria' => $key->nama,
                            'nilai' => (int)$key->nilai
                        ];
                        array_push($profilNasabah, $obj);
                    }
                }
            }
            if($getPilihan){
                foreach ($getPilihan as $keys ) {
                    if (str_contains($value->nilai, $keys->nama_kategori)) {

                        $obj = (object) [
                            'nama_produk' => auth()->user()->name,
                            'sub_kriteria' => $keys->sub_nama,
                            'nilai' => (int)$keys->kategori_nilai
                        ];
                        array_push($profilNasabah, $obj);
                    }
                }
            }
        }
        // print("<pre>". print_r($profilNasabah, true) ."</pre>");
        // dd($profilNasabah);
        $getKriteria = Kriteria::all();
        $getProduk = Produk::orderBy('created_at', 'desc')->paginate(100);

        //looping nilai nasabah sebanyak jumlah produk
        $getProduks = Produk::all();
        // $getSub = SubKriteria::all();
        $getSub = DB::table('tbl_pemilihan')->where('customer', 'LIKE', $customer)->where('kode_pemilihan', 'LIKE', $data)->get();
        $arx = array();
        foreach($getProduks as $i => $values){
            foreach($getSub as $j => $valusss){
                // print_r($j);
                $t = $profilNasabah[$j]->nilai;
                array_push($arx, $t);
            }
        }


        // return($getInputData);

        return view('Customer.Hasil-Pemilihan.get-hasil', compact('getInputData', 'getKriteria', 'getProduk', 'profilNasabah','arx', 'kode_pemilihan'));
    }
}
