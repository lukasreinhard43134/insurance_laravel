<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\SubKriteria;
use App\Models\Kriteria;
use Illuminate\Http\Request;
use SebastianBergmann\Environment\Console;

class SubKriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtSubKriteria = SubKriteria::with('kriteria')->orderBy('created_at', 'desc')->paginate(10);
        $dtSubKriteriaDetail = Kategori::all();
        // dd($dtSubKriteria);
        return view('SubKriteria.dataSubKriteria', compact('dtSubKriteria', 'dtSubKriteriaDetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dtSubKriteria = Kriteria::all();
        return view('SubKriteria.createSubKriteria', compact('dtSubKriteria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'kriteria' => 'required',
            'nama' => 'required|unique:sub_kriteria,nama',
            'ranking' => 'required|numeric|unique:sub_kriteria,ranking',
            'bobot' => 'required|numeric'
        ]);
        SubKriteria::create([
            'id_kriteria' => $request->kriteria,
            'nama' => $request->nama,
            'ranking' => $request->ranking,
            'bobot' => $request->bobot,
            'tipe_input' => $request->tipe_input
        ]);
        return redirect('data-sub-kriteria')->with('toast_success', 'Data Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subkrite = SubKriteria::findorfail($id);
        return view('SubKriteria.editSubKriteria', compact('subkrite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $subkrite = SubKriteria::findorfail($id);
        $request->validate([
            'nama' => 'required',
            'ranking' => 'required|numeric',
            'bobot' => 'required|numeric'
        ]);
        $subkrite->update([
            'nama' => $request->nama,
            'ranking' => $request->ranking,
            'bobot' => $request->bobot,
            'tipe_input' => $request->tipe_input
        ]);
        return redirect('data-sub-kriteria')->with('toast_success', 'Data Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subKrite = SubKriteria::findorfail($id);
        $subKrite->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }
}
