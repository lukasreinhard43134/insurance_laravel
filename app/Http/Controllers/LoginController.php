<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PhpParser\Builder\Function_;
use PhpParser\Node\Expr\FuncCall;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use App\Models\User;
use Illuminate\Contracts\Session\Session;

class LoginController extends Controller
{
    public function postLogin(Request $request){

        if(Auth::attempt($request->only('email', 'password'))){
            $isLogin = 'true';
            $roleSession = auth()->user()->role;
            $nameSession = auth()->user()->name;
            // $request->session()->forget('isGuest');
            $request->session()->put('isLogin', $isLogin);
            $request->session()->put('isName', $nameSession);
            $request->session()->put('isRole', $roleSession);
            // dd($request->session()->get('isName'));
            // dd(auth()->user());
            return redirect('/beranda');
        }
        toast('email or password is not correct !','error')->autoClose(3000);
        return redirect('/login');
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->forget('isLogin');
        $request->session()->forget('isName');
        $request->session()->forget('isRole');
        // $request->session()->forget('isGuest');
        return redirect('/beranda');
    }

    public function login(Request $request){
        return view('Pengguna.login');
    }


}
