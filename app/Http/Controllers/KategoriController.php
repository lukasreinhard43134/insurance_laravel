<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\SubKriteria;
use Illuminate\Contracts\Validation\Rule;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = Kategori::orderBy('created_at', 'desc')->paginate(5);
        return view('Kategori.dataKategori', compact('getData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getSubKriteria = SubKriteria::all();
        return view('Kategori.createKategori',compact('getSubKriteria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            // 'subKriteria' => 'required',
            'nama' => 'required|unique:kategori,nama',
            'nilai' => 'required|unique:kategori,nilai,NULL,id,nama,nama'
        ]);

        $getTipeInput = DB::table('sub_kriteria')->where('nama', 'LIKE', $request->subKriteria)->get();
        if($getTipeInput[0]->tipe_input === 'pilihan'){
            Kategori::create([
                'sub_kriteria_nama' => $request->subKriteria,
                'nama' => $request->nama,
                'nilai' => $request->nilai,
                'nilai_awal' => '',
                'nilai_akhir' => ''

            ]);
        }else{
            $nilai = $this->getNilaiAwalAkhir($request->nama);
            dd($nilai);
            Kategori::create([
                'sub_kriteria_nama' => $request->subKriteria,
                'nama' => $request->nama,
                'nilai' => $request->nilai,
                'nilai_awal' => $nilai['nilai_awal'],
                'nilai_akhir'=> $nilai['nilai_akhir']
            ]);
        }
        toast('Data Created Successfully','success')->autoClose(3000);
        return redirect('data-kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getDataByid = Kategori::findorfail($id);
        return view('Kategori.editKategori', compact('getDataByid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'subKriteria' => 'required',
            'nama' => 'required',
            'nilai' => 'required|numeric'
        ]);
        $getDetailSubKriteria = Kategori::findorfail($id);
        $getDetailSubKriteria->update([
            'sub_kriteria_nama' => $request->subKriteria,
            'nama' => $request->nama,
            'nilai'=> $request->nilai
        ]);
        toast('Data Update Successfully','success')->autoClose(3000);
        return redirect('data-kategori');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $getSubKriteriaDetail = Kategori::findorfail($id);
        $getSubKriteriaDetail->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }

    public function getNilaiAwalAkhir($data){
        if(str_contains($data, '>')){
            if(str_contains($data, '-')){
                $specialChar = '-';
                $getIndexSpecialChar = stripos($data, $specialChar);
                $nilaiAwal = substr($data, 0, $getIndexSpecialChar);
                $x = substr($data, $getIndexSpecialChar + 1);
                if(str_contains($x, '>')){
                    $specialChar2 = '>';
                    $getIndexSpecialChar2 = stripos($x, $specialChar2);
                    $nilaiAkhir = substr($x, $getIndexSpecialChar2 + 1);
                }
            }else{
                $string = $data;
                $sliceString = substr($string, 1);
                $toInt = (int)$sliceString;
                $nilaiAwal = $toInt + 1;
                $nilaiAkhir = 9999999999999999999999;
            }
        }else if(str_contains($data, '-')){
            $specialChar = '-';
            $getIndexSpecialChar = stripos($data, $specialChar);
            $nilaiAwal = substr($data, 0, $getIndexSpecialChar);
            $x = substr($data, $getIndexSpecialChar + 1);
            if(str_contains($x, '<=')){
                $specialChar2 = '<=';
                $getIndexSpecialChar2 = stripos($x, $specialChar2);
                $nilaiAkhir = substr($x, $getIndexSpecialChar2 + 2);
            }else if(str_contains($x, '=')){
                $specialChar2 = '=';
                $getIndexSpecialChar2 = stripos($x, $specialChar2);
                $nilaiAkhir = substr($x, $getIndexSpecialChar2 + 1);
            }else if(str_contains($x, '>')){
                $specialChar2 = '>';
                $getIndexSpecialChar2 = stripos($x, $specialChar2);
                $nilaiAkhir = substr($x, $getIndexSpecialChar2 + 1);
            }else if(str_contains($x, '<')){
                $specialChar2 = '<';
                $getIndexSpecialChar2 = stripos($x, $specialChar2);
                $nilaiAkhir = substr($x, $getIndexSpecialChar2 + 1);
            }else{
                $nilaiAkhir= substr($data, $getIndexSpecialChar + 1);
            }
        }else{
            $nilaiAwal = null;
            $nilaiAkhir = null;
        }

        $out= [
            'nilai_awal'  =>  $nilaiAwal,
            'nilai_akhir' =>  $nilaiAkhir
        ];
        return($out);
    }
}
