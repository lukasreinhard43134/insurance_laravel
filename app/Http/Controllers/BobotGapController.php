<?php

namespace App\Http\Controllers;

use App\Models\BobotGap;
use Illuminate\Http\Request;

class BobotGapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtBobotGap = BobotGap::paginate(7);
        return view('BobotGap.dataBobotGap', compact('dtBobotGap'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dtBobotGap = BobotGap::all();
        return view('BobotGap.createBobotGap', compact('dtBobotGap'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'selisih' => 'required|numeric',
            'bobot' => 'required|numeric',
            'keterangan' => 'required'
        ]);
        BobotGap::create([
            'selisih' => $request->selisih,
            'bobot' => $request->bobot,
            'keterangan' => $request->keterangan
        ]);
        return redirect('data-bobot-gap')->with('toast_success', 'Data Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bobotgap = BobotGap::findorfail($id);
        return view('BobotGap.editBobotGap', compact('bobotgap'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bobotgap = BobotGap::findorfail($id);

        $request->validate([
            'selisih' => 'required|numeric',
            'bobot' => 'required|numeric',
            'keterangan' => 'required'
        ]);

        $bobotgap->update([
            'selisih' => $request->selisih,
            'bobot' => $request->bobot,
            'keterangan' => $request->keterangan,
        ]);
        return redirect('data-bobot-gap')->with('toast_success', 'Data Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bobotgapp = BobotGap::findorfail($id);
        $bobotgapp->delete();
        return back()->with('info', 'Data Deleted Successfully!');
    }
}
