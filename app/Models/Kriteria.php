<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kriteria extends Model
{
    // use HasFactory;
    protected $table = "kriteria";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'nama_kriteria', 'ranking', 'bobot_roc'
    ];

    public function subkriteria()
    {
        return $this->hasMany(SubKriteria::class);
    }

    public function scopeTest(){
        // $dat = DB::table('kriteria')->get()->count();
        $dat = DB::table('kriteria')->get('id');
        return($dat);
    }
}
