<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Kriteria;

class SubKriteria extends Model
{
    // use HasFactory;
    protected $table = "sub_kriteria";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'id_kriteria', 'nama', 'ranking', 'bobot'
    ];

    public function kriteria()
    {
        return $this->belongsTo(Kriteria::class, 'id_kriteria', 'id');
    }
}
