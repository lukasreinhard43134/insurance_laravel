<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class hasilPemilihan extends Model
{
    use HasFactory;
    protected $table = "hasil_pemilihan";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'kode_pemilihan','nama_produk','nilai','customer'
    ];
}
