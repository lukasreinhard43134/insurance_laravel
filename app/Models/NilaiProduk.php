<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NilaiProduk extends Model
{
    use HasFactory;
    protected $table = "nilai_produk";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'nama_produk', 'nama', 'nilai'
    ];
}
