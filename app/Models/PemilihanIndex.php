<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PemilihanIndex extends Model
{
    use HasFactory;
    protected $table = "pemilihan_index";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'customer', 'kode_pemilihan'
    ];
}
