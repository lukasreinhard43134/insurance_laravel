<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemilihan extends Model
{
    use HasFactory;
    protected $table = "tbl_pemilihan";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'kode_pemilihan', 'nama','nilai', 'customer'
    ];
}
