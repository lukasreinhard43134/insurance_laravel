<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class customer extends Model
{
    use HasFactory;
    protected $table = "tbl_customer";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'fullname', 'alamat', 'no_handphone', 'email'
    ];

    // public function user()
    // {
    //     return $this->belongsTo(User::class, 'user_id', 'id');
    // }
}
