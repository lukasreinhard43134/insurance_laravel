<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    // Auth::logout();
    return view('beranda');
    // return redirect('/login');
});

Route::get('/beranda', 'BerandaController@index')->name('beranda');
Route::get('/error-403', 'BerandaController@forbidden')->name('error-403');


//handling error route
// Route::fallback(function () {
//     return view('Template.notFound');
// });

//register
Route::get('/register', function () {
    return view('Pengguna.register');
})->name('register');
Route::post('/postRegister', 'UserController@store')->name('postRegister');


//login and logout
Route::get('/login', function () {
    return view('Pengguna.login');
})->name('login');

Route::post('/postLogin', 'LoginController@postLogin')->name('postLogin');
Route::get('/logout', 'LoginController@logout')->name('logout');





//only admin
Route::middleware(['auth', 'ceklevel:admin'])->group(function () {
    //produk
    // Route::get('/data-produk', 'ProdukController@index')->name('data-produk');
    Route::get('/create-produk', 'ProdukController@create')->name('create-produk');
    Route::post('/simpan-produk', 'ProdukController@store')->name('simpan-produk');
    Route::get('/edit-produk/{id}', 'ProdukController@edit')->name('edit-produk');
    Route::post('/update-produk/{id}', 'ProdukController@update')->name('update-produk');
    Route::get('/delete-produk/{id}', 'ProdukController@destroy')->name('delete-produk');

    //kriteria
    Route::get('/data-kriteria', 'KriteriaController@index')->name('data-kriteria');
    Route::get('/create-kriteria', 'KriteriaController@create')->name('create-kriteria');
    Route::post('/simpan-kriteria', 'KriteriaController@store')->name('simpan-kriteria');
    Route::get('/edit-kriteria/{id}', 'KriteriaController@edit')->name('edit-kriteria');
    Route::post('/update-kriteria/{id}', 'KriteriaController@update')->name('update-kriteria');
    Route::get('/delete-kriteria/{id}', 'KriteriaController@destroy')->name('delete-kriteria');

    //sub-kriteria
    Route::get('/data-sub-kriteria', 'SubKriteriaController@index')->name('data-sub-kriteria');
    Route::get('/create-sub-kriteria', 'SubKriteriaController@create')->name('create-sub-kriteria');
    Route::post('/simpan-sub-kriteria', 'SubKriteriaController@store')->name('simpan-sub-kriteria');
    Route::get('/edit-sub-kriteria/{id}', 'SubKriteriaController@edit')->name('edit-sub-kriteria');
    Route::post('/update-sub-kriteria/{id}', 'SubKriteriaController@update')->name('update-sub-kriteria');
    Route::get('/delete-sub-kriteria/{id}', 'SubKriteriaController@destroy')->name('delete-sub-kriteria');

    //bobot-gap
    Route::get('/data-bobot-gap', 'BobotGapController@index')->name('data-bobot-gap');
    Route::get('/create-bobot-gap', 'BobotGapController@create')->name('create-bobot-gap');
    Route::post('/simpan-bobot-gap', 'BobotGapController@store')->name('simpan-bobot-gap');
    Route::get('/edit-bobot-gap/{id}', 'BobotGapController@edit')->name('edit-bobot-gap');
    Route::post('/update-bobot-gap/{id}', 'BobotGapController@update')->name('update-bobot-gap');
    Route::get('/delete-bobot-gap/{id}', 'BobotGapController@destroy')->name('delete-bobot-gap');

    //kategori-detail
    Route::get('/data-kategori', 'KategoriController@index')->name('data-kategori');
    Route::get('/create-kategori', 'KategoriController@create')->name('create-kategori');
    Route::post('/simpan-kategori', 'KategoriController@store')->name('simpan-kategori');
    Route::get('/edit-kategori/{id}', 'KategoriController@edit')->name('edit-kategori');
    Route::post('/update-kategori/{id}', 'KategoriController@update')->name('update-kategori');
    Route::get('/delete-kategori/{id}', 'KategoriController@destroy')->name('delete-kategori');

    //nilai-produk
    Route::get('/data-nilai-produk', 'NilaiProdukController@index')->name('data-nilai-produk');
    Route::post('/simpan-data-nilai', 'NilaiProdukController@store')->name('simpan-data-nilai');
    Route::post('/update-data-nilai/{id}', 'NilaiProdukController@update')->name('update-data-nilai');

    //data-nasabah
    Route::get('/data-nasabah', 'CustomerController@index')->name('data-nasabah');
    Route::post('/update-data-nasabah/{id}', 'CustomerController@update')->name('update-data-nasabah');
    Route::get('/delete-data-nasabah/{id}', 'CustomerController@destroy')->name('delete-data-nasabah');

    //hasil-pemilihan admin
    Route::get('/data-hasil-pemilihan-admin', 'PemilihanController@DataHasilPemilihanAdmin')->name('data-hasil-pemilihan-admin');
});

Route::middleware(['auth', 'ceklevel:admin,customer'])->group(function () {
    // Route::get('/beranda', 'BerandaController@index');
    Route::get('/data-produk', 'ProdukController@index')->name('data-produk');

    //account setting
    Route::get('/account-setting/{id}', 'UserController@accountSetting')->name('account-setting');
    Route::get('/detail-account/{id}', 'UserController@getDetailAccount')->name('detail-account');
    Route::post('/update-account/{id}', 'UserController@updateAccount')->name('update-account');
    Route::get('/delete-account/{id}', 'UserController@deleteAccount')->name('delete-account');
    Route::get('/change-password/{id}', 'UserController@changePassUser')->name('change-password');
    Route::post('/simpan-data-change-password/{id}', 'UserController@simpanPassUser')->name('simpan-data-change-password');

    //get hasil pemilihan
    Route::get('/data-hasil-pemilihan', 'HasilPemilihanController@index')->name('data-hasil-pemilihan');
    Route::get('/data-detail-hasil-pemilihan', 'HasilPemilihanController@indexDetail')->name('data-detail-hasil-pemilihan');
    Route::get('/data-submit-hasil-pemilihan', 'HasilPemilihanController@indexSubmit')->name('data-submit-hasil-pemilihan');
    Route::get('/detail-hasil/{id}/{customer}', 'PemilihanController@getDetailHasil')->name('detail-hasil');
    Route::get('/delete-hasil/{id}/{customer}', 'PemilihanController@deleteHasil')->name('delete-hasil');
});


//only customer
Route::middleware(['auth', 'ceklevel:customer'])->group(function () {
    // Route::get('/beranda', 'BerandaController@index');
    // Route::get('/customer/produk/index', 'ProdukController@indexCustomer')->name('customer/produk/index');

    Route::get('/data-pemilihan', 'pemilihanController@index')->name('data-pemilihan');
    Route::post('/simpan-pemilihan', 'pemilihanController@store')->name('simpan-pemilihan');

});
