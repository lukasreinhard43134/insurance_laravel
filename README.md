<h1>Application Website Asuransi using Laravel</h1>

## Requirements 
- [Xampp](https://www.apachefriends.org/download.html).
- [VisualStudioCode](https://code.visualstudio.com/download).
- [Chrome](https://www.google.com/chrome/).
- [GitBash](https://git-scm.com/download/win).
- [Sourcetree](https://www.sourcetreeapp.com/).

## How to Configure Application in your local
- install the requirements first
- clone the [repository](https://gitlab.com/lukasreinhard43134/insurance_laravel).
- Open apps that has been clone in visual studio code
- Open Xampp and running Apache & Mysql on Xampp
- Create database with name 'laravel' on [Mysql](http://localhost/phpmyadmin/index.php)
- On visual studio code :
    - Type 'composer install'
    - rename .env.example to .env
    - Type 'php artisan migrate' to migrate db for application
    - Type 'php artisan serve' to run the applications
- Access on browser for the [Application](http://localhost:8000/).  
