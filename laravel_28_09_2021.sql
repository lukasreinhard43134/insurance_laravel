-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2021 at 03:27 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bobot_gap`
--

CREATE TABLE `bobot_gap` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `selisih` double NOT NULL,
  `bobot` double NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bobot_gap`
--

INSERT INTO `bobot_gap` (`id`, `selisih`, `bobot`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 0, 5, 'tidak ada GAP (kompetensi sesuai)', '2021-07-31 15:35:40', '2021-07-31 15:35:40'),
(2, 1, 4.5, 'kompetensi individu kelebihan 1 tingkat/level', '2021-07-31 15:37:09', '2021-07-31 15:37:09'),
(14, -1, 4, 'kompetensi individu kurang 1 tingkat/level', '2021-07-31 16:30:56', '2021-07-31 16:30:56'),
(15, 2, 3.5, 'kompetensi individu kelebihan 2 tingkat/level', '2021-07-31 16:31:42', '2021-07-31 16:31:42'),
(16, -2, 3, 'kompetensi individu kurang 2 tingkat/level', '2021-07-31 16:32:08', '2021-07-31 16:32:08'),
(17, 3, 2.5, 'kompetensi individu kelebihan 3 tingkat/level', '2021-07-31 16:32:29', '2021-07-31 16:32:29'),
(18, -3, 2, 'kompetensi individu kurang 3 tingkat/level', '2021-07-31 16:32:47', '2021-07-31 16:32:47'),
(19, 4, 1.5, 'kompetensi individu kelebihan 4 tingkat/level', '2021-07-31 16:33:10', '2021-07-31 16:33:10'),
(20, -4, 1, 'kompetensi individu kurang 4 tingkat/level', '2021-07-31 16:33:34', '2021-07-31 16:33:34');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_pemilihan`
--

CREATE TABLE `hasil_pemilihan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_pemilihan` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hasil_pemilihan`
--

INSERT INTO `hasil_pemilihan` (`id`, `kode_pemilihan`, `nama_produk`, `nilai`, `customer`, `created_at`, `updated_at`) VALUES
(199, 'smarter_1', 'produk 5', '8.681', 'nasabah10', '2021-09-20 00:47:12', '2021-09-20 00:47:12'),
(200, 'smarter_1', 'produk 4', '9.6945', 'nasabah10', '2021-09-20 00:47:12', '2021-09-20 00:47:12'),
(201, 'smarter_1', 'produk 7', '7.653', 'nasabah10', '2021-09-20 00:47:12', '2021-09-20 00:47:12'),
(202, 'smarter_1', 'qwdwq', '7.778', 'nasabah10', '2021-09-20 00:47:12', '2021-09-20 00:47:12'),
(203, 'smarter_1', 'Asuransi Perlindungan Dana Pendidikan Anak', '8.403', 'nasabah10', '2021-09-20 00:47:12', '2021-09-20 00:47:12'),
(204, 'smarter_2', 'produk 5', '8.986', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(205, 'smarter_2', 'produk 4', '8.708', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(206, 'smarter_2', 'produk 7', '8.931', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(207, 'smarter_2', 'qwdwq', '9.056', 'nasabah10', '2021-09-20 00:48:22', '2021-09-20 00:48:22'),
(208, 'smarter_2', 'Asuransi Perlindungan Dana Pendidikan Anak', '8.556', 'nasabah10', '2021-09-20 00:48:22', '2021-09-20 00:48:22'),
(209, 'smarter_1', 'produk 5', '8.569', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(210, 'smarter_1', 'produk 4', '8.75', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(211, 'smarter_1', 'produk 7', '8.514', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(212, 'smarter_1', 'qwdwq', '8.639', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(213, 'smarter_1', 'Asuransi Perlindungan Dana Pendidikan Anak', '9.264', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(214, 'smarter_3', 'Asuransi Proteksi Income', '9.0415', 'nasabah10', '2021-09-20 07:49:33', '2021-09-20 07:49:33'),
(215, 'smarter_3', 'produk 5', '8.681', 'nasabah10', '2021-09-20 07:49:33', '2021-09-20 07:49:33'),
(216, 'smarter_3', 'produk 4', '9.6945', 'nasabah10', '2021-09-20 07:49:33', '2021-09-20 07:49:33'),
(217, 'smarter_3', 'produk 7', '7.653', 'nasabah10', '2021-09-20 07:49:33', '2021-09-20 07:49:33'),
(218, 'smarter_3', 'qwdwq', '7.778', 'nasabah10', '2021-09-20 07:49:33', '2021-09-20 07:49:33');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_kriteria_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL,
  `nilai_awal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai_akhir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `sub_kriteria_nama`, `nama`, `nilai`, `nilai_awal`, `nilai_akhir`, `created_at`, `updated_at`) VALUES
(1, 'pekerjaan', 'Wirausaha', 1, '', '', '2021-08-11 11:54:48', '2021-08-11 11:54:48'),
(4, 'pekerjaan', 'Pilot/Pramugari', 2, '', '', '2021-08-11 11:59:54', '2021-08-11 11:59:54'),
(6, 'usia', '20-30', 5, '20', '30', '2021-08-12 02:20:04', '2021-08-12 02:20:04'),
(7, 'usia', '31-40', 3, '31', '40', '2021-08-12 02:20:34', '2021-08-12 02:20:34'),
(8, 'pendapatan', '0 - <= 2.500.000', 1, '0', '2500000', '2021-08-13 07:22:58', '2021-08-13 07:22:58'),
(9, 'pendapatan', '2.500.001 – 5.000.000', 2, '2500001', '5000000', '2021-08-13 07:23:49', '2021-08-13 07:23:49'),
(10, 'status_pernikahan', 'Janda/duda', 2, '', '', '2021-08-13 07:25:35', '2021-08-13 07:25:35'),
(11, 'status_pernikahan', 'Menikah', 3, '', '', '2021-08-13 07:25:58', '2021-08-13 07:25:58'),
(12, 'jumlah_anak', '>3', 2, '4', '9999999999999999999999', '2021-08-13 07:26:23', '2021-08-13 07:26:23'),
(13, 'jumlah_anak', '2 - 3', 3, '2', '3', '2021-08-13 07:26:46', '2021-08-13 07:26:46'),
(14, 'jumlah_anak', '0-1', 5, '0', '1', '2021-08-15 01:59:52', '2021-08-15 01:59:52'),
(15, 'pekerjaan', 'Dokter', 5, '', '', '2021-08-15 02:00:37', '2021-08-15 02:00:37'),
(17, 'pekerjaan', 'PNS/TNI/POLRI', 6, '', '', '2021-08-15 11:36:52', '2021-08-15 11:36:52'),
(18, 'pendapatan', '5.000.001 – 7.500.000', 3, '5000001', '7500000', '2021-08-15 12:20:23', '2021-08-15 12:20:23'),
(19, 'pekerjaan', 'pegawai swasta', 4, '', '', '2021-09-24 03:56:38', '2021-09-24 03:56:38'),
(21, 'pekerjaan', 'Wiraswasta', 3, '', '', '2021-09-24 04:55:27', '2021-09-24 04:55:27'),
(28, 'usia', '41-50', 2, '41', '50', '2021-09-24 05:05:21', '2021-09-24 05:05:21'),
(29, 'usia', '>50', 1, '51', '9999999999999999999999', '2021-09-24 05:06:34', '2021-09-24 05:06:34'),
(30, 'pendapatan', '7500001-10000000', 4, '7500001', '10000000', '2021-09-24 05:08:54', '2021-09-24 05:08:54'),
(31, 'pendapatan', '>10000000', 5, '10000001', '9999999999999999999999', '2021-09-24 05:09:29', '2021-09-24 05:09:29'),
(33, 'status_pernikahan', 'belum menikah', 5, '', '', '2021-09-24 06:01:48', '2021-09-24 06:01:48');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kriteria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ranking` int(11) NOT NULL,
  `bobot_roc` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id`, `nama_kriteria`, `ranking`, `bobot_roc`, `created_at`, `updated_at`) VALUES
(1, 'Khusus', 1, 0.75, '2021-07-29 18:03:30', '2021-07-29 18:16:34'),
(2, 'Umum', 2, 0.25, '2021-07-29 18:04:36', '2021-07-29 18:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_07_21_085341_create_produks_table', 2),
(5, '2021_07_30_002301_create_kriterias_table', 3),
(6, '2021_07_30_062724_create_sub_kriterias_table', 4),
(7, '2021_07_31_021516_create_bobot_gaps_table', 5),
(8, '2021_08_03_032648_create_kategoris_table', 6),
(9, '2021_08_03_155743_create_nilai_produks_table', 7),
(10, '2021_08_06_134201_create_customers_table', 8),
(11, '2021_08_13_154934_create_pemilihans_table', 9),
(12, '2021_09_04_030453_create_nilai_utilities_table', 10),
(13, '2021_09_19_104517_create_nama_models_table', 11),
(14, '2021_09_19_104607_create_tbl_hasil_pemilihans_table', 11),
(15, '2021_09_19_105308_create_hasil_pemilihans_table', 12),
(16, '2021_09_19_113646_create_pemilihan_indices_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_produk`
--

CREATE TABLE `nilai_produk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nilai_produk`
--

INSERT INTO `nilai_produk` (`id`, `nama_produk`, `nama`, `nilai`, `created_at`, `updated_at`) VALUES
(31, 'produk 7', 'pendapatan', '1', '2021-08-15 12:07:58', '2021-08-15 12:07:58'),
(32, 'produk 7', 'usia', '5', '2021-08-15 12:07:58', '2021-08-15 12:07:58'),
(33, 'produk 7', 'pekerjaan', '2', '2021-08-15 12:07:58', '2021-08-15 12:07:58'),
(34, 'produk 7', 'status_pernikahan', '2', '2021-08-15 12:07:58', '2021-08-15 12:07:58'),
(35, 'produk 7', 'jumlah_anak', '3', '2021-08-15 12:07:58', '2021-08-15 12:07:58'),
(36, 'qwdwq', 'pendapatan', '1', '2021-08-15 12:09:54', '2021-08-15 12:09:54'),
(37, 'qwdwq', 'usia', '5', '2021-08-15 12:09:54', '2021-08-15 12:09:54'),
(38, 'qwdwq', 'pekerjaan', '2', '2021-08-15 12:09:54', '2021-08-15 12:09:54'),
(39, 'qwdwq', 'status_pernikahan', '2', '2021-08-15 12:09:54', '2021-08-15 12:09:54'),
(40, 'qwdwq', 'jumlah_anak', '2', '2021-08-15 12:09:54', '2021-08-15 12:09:54'),
(41, 'Asuransi Perlindungan Dana Pendidikan Anak', 'pendapatan', '1', '2021-08-15 12:10:19', '2021-08-15 12:10:19'),
(42, 'Asuransi Perlindungan Dana Pendidikan Anak', 'usia', '5', '2021-08-15 12:10:19', '2021-08-15 12:10:19'),
(43, 'Asuransi Perlindungan Dana Pendidikan Anak', 'pekerjaan', '2', '2021-08-15 12:10:19', '2021-08-15 12:10:19'),
(44, 'Asuransi Perlindungan Dana Pendidikan Anak', 'status_pernikahan', '3', '2021-08-15 12:10:19', '2021-08-15 12:10:19'),
(45, 'Asuransi Perlindungan Dana Pendidikan Anak', 'jumlah_anak', '3', '2021-08-15 12:10:19', '2021-08-15 12:10:19'),
(57, 'produk 4', 'pendapatan', '3', '2021-08-26 04:23:15', '2021-08-26 04:23:15'),
(58, 'produk 4', 'usia', '3', '2021-08-26 04:23:15', '2021-08-26 04:23:15'),
(59, 'produk 4', 'pekerjaan', '6', '2021-08-26 04:23:15', '2021-08-26 04:23:15'),
(60, 'produk 4', 'status_pernikahan', '3', '2021-08-26 04:23:15', '2021-08-26 04:23:15'),
(61, 'produk 4', 'jumlah_anak', '2', '2021-08-26 04:23:15', '2021-08-26 04:23:15'),
(62, 'produk 5', 'pendapatan', '2', '2021-09-06 05:51:18', '2021-09-06 05:51:18'),
(63, 'produk 5', 'usia', '3', '2021-09-06 05:51:18', '2021-09-06 05:51:18'),
(64, 'produk 5', 'pekerjaan', '2', '2021-09-06 05:51:18', '2021-09-06 05:51:18'),
(65, 'produk 5', 'status_pernikahan', '2', '2021-09-06 05:51:18', '2021-09-06 05:51:18'),
(66, 'produk 5', 'jumlah_anak', '3', '2021-09-06 05:51:18', '2021-09-06 05:51:18'),
(67, 'Asuransi Proteksi Income', 'pendapatan', '3', '2021-09-20 07:47:43', '2021-09-20 07:47:43'),
(68, 'Asuransi Proteksi Income', 'usia', '5', '2021-09-20 07:47:43', '2021-09-20 07:47:43'),
(69, 'Asuransi Proteksi Income', 'pekerjaan', '5', '2021-09-20 07:47:43', '2021-09-20 07:47:43'),
(70, 'Asuransi Proteksi Income', 'status_pernikahan', '3', '2021-09-20 07:47:43', '2021-09-20 07:47:43'),
(71, 'Asuransi Proteksi Income', 'jumlah_anak', '3', '2021-09-20 07:47:43', '2021-09-20 07:47:43');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_utility`
--

CREATE TABLE `nilai_utility` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_kriteria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pemilihan_index`
--

CREATE TABLE `pemilihan_index` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pemilihan` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pemilihan_index`
--

INSERT INTO `pemilihan_index` (`id`, `customer`, `kode_pemilihan`, `created_at`, `updated_at`) VALUES
(84, 'nasabah10', 'smarter_1', '2021-09-20 00:47:11', '2021-09-20 00:47:11'),
(85, 'nasabah10', 'smarter_2', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(86, 'nasabah2', 'smarter_1', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(87, 'nasabah10', 'smarter_3', '2021-09-20 07:49:32', '2021-09-20 07:49:32');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` bigint(20) UNSIGNED NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `keterangan_produk`, `created_at`, `updated_at`) VALUES
(12, 'Asuransi Perlindungan Dana Pendidikan Anak', '5', '2021-08-03 08:02:10', '2021-08-03 08:02:10'),
(13, 'qwdwq', 'qwdq', '2021-08-08 05:53:53', '2021-08-08 05:53:53'),
(14, 'produk 7', 'ini keterangan produk 7', '2021-08-15 01:58:00', '2021-08-15 01:58:00'),
(16, 'produk 4', 'ini adalah produk 4', '2021-08-26 03:32:35', '2021-09-24 07:57:22'),
(17, 'produk 5', 'ini adalah keterangan produk 5', '2021-09-06 05:50:32', '2021-09-25 02:41:15'),
(18, 'Asuransi Proteksi Income', 'ini adalah Asuransi Proteksi Income', '2021-09-20 07:47:04', '2021-09-25 02:41:03');

-- --------------------------------------------------------

--
-- Table structure for table `sub_kriteria`
--

CREATE TABLE `sub_kriteria` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_kriteria` bigint(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ranking` int(11) NOT NULL,
  `bobot` double NOT NULL,
  `tipe_input` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pilihan',
  `tipe_input_2` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pilihan',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_kriteria`
--

INSERT INTO `sub_kriteria` (`id`, `id_kriteria`, `nama`, `ranking`, `bobot`, `tipe_input`, `tipe_input_2`, `created_at`, `updated_at`) VALUES
(1, 1, 'pendapatan', 1, 0.611, 'input_currency', 'pilihan', '2021-07-30 01:17:50', '2021-08-01 02:05:28'),
(2, 1, 'usia', 2, 0.278, 'input', 'pilihan', '2021-07-30 01:20:31', '2021-07-30 10:09:51'),
(3, 1, 'pekerjaan', 3, 0.111, 'pilihan', 'pilihan', '2021-07-30 02:20:22', '2021-07-30 02:20:22'),
(4, 2, 'status_pernikahan', 4, 0.75, 'pilihan', 'pilihan', '2021-07-30 10:24:15', '2021-07-30 10:24:15'),
(5, 2, 'jumlah_anak', 5, 0.25, 'input', 'pilihan', '2021-07-30 10:25:19', '2021-07-30 10:25:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_handphone` bigint(25) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `fullname`, `alamat`, `no_handphone`, `email`, `created_at`, `updated_at`) VALUES
(10, 'nasabah2', 'jl nasabah2', 852615763433, 'nasabah2@gmail.com', '2021-08-15 03:30:34', '2021-09-20 02:34:16'),
(11, 'nasabah10', 'jl. nasabah10', 85261576341, 'nasabah10@gmail.com', '2021-08-15 03:38:29', '2021-08-15 03:39:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pemilihan`
--

CREATE TABLE `tbl_pemilihan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_pemilihan` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_pemilihan`
--

INSERT INTO `tbl_pemilihan` (`id`, `kode_pemilihan`, `nama`, `nilai`, `customer`, `created_at`, `updated_at`) VALUES
(406, 'smarter_1', 'pendapatan', 'Rp 5.000.000', 'nasabah10', '2021-09-20 00:47:11', '2021-09-20 00:47:11'),
(407, 'smarter_1', 'usia', '40', 'nasabah10', '2021-09-20 00:47:11', '2021-09-20 00:47:11'),
(408, 'smarter_1', 'pekerjaan', 'PNS/TNI/POLRI', 'nasabah10', '2021-09-20 00:47:11', '2021-09-20 00:47:11'),
(409, 'smarter_1', 'status_pernikahan', 'Menikah', 'nasabah10', '2021-09-20 00:47:12', '2021-09-20 00:47:12'),
(410, 'smarter_1', 'jumlah_anak', '6', 'nasabah10', '2021-09-20 00:47:12', '2021-09-20 00:47:12'),
(411, 'smarter_2', 'pendapatan', 'Rp 5.000.000', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(412, 'smarter_2', 'usia', '30', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(413, 'smarter_2', 'pekerjaan', 'Dokter', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(414, 'smarter_2', 'status_pernikahan', 'Janda/duda', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(415, 'smarter_2', 'jumlah_anak', '5', 'nasabah10', '2021-09-20 00:48:21', '2021-09-20 00:48:21'),
(416, 'smarter_1', 'pendapatan', 'Rp 5.000.000', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(417, 'smarter_1', 'usia', '24', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(418, 'smarter_1', 'pekerjaan', 'Pilot/Pramugari', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(419, 'smarter_1', 'status_pernikahan', 'Menikah', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(420, 'smarter_1', 'jumlah_anak', '4', 'nasabah2', '2021-09-20 00:49:53', '2021-09-20 00:49:53'),
(421, 'smarter_3', 'pendapatan', 'Rp 5.000.000', 'nasabah10', '2021-09-20 07:49:32', '2021-09-20 07:49:32'),
(422, 'smarter_3', 'usia', '37', 'nasabah10', '2021-09-20 07:49:32', '2021-09-20 07:49:32'),
(423, 'smarter_3', 'pekerjaan', 'PNS/TNI/POLRI', 'nasabah10', '2021-09-20 07:49:32', '2021-09-20 07:49:32'),
(424, 'smarter_3', 'status_pernikahan', 'Menikah', 'nasabah10', '2021-09-20 07:49:32', '2021-09-20 07:49:32'),
(425, 'smarter_3', 'jumlah_anak', '4', 'nasabah10', '2021-09-20 07:49:32', '2021-09-20 07:49:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin1', 'admin', 'admin@gmail.com', NULL, '$2y$10$scrsNrKsFHdOhUnFmg0.tOxIiI9Xt0p1MnbI2l4J8M.z7VAZV5/Kq', 'PebqH2Arc7PMhbrcd8DmotwL0wPtDgmPtDnNIuOEDlSERBvpW7D97pFBEpWW', '2021-08-06 11:13:34', '2021-09-20 06:49:13'),
(10, 'nasabah2', 'customer', 'nasabah2@gmail.com', NULL, '$2y$10$Z1tfxlYHIBlw7/fDem/mJOYcvvAKH/zCqSbfzR1DvGZX.TYkg8IsO', 'f9Xdym7vohq6SAAdUUZ3IcVvxcnsDzW9CqmSguL9UJ5IPiy9onORqz5nTeJs', '2021-08-15 03:30:34', '2021-09-20 07:00:27'),
(11, 'nasabah10', 'customer', 'nasabah10@gmail.com', NULL, '$2y$10$2AT92IAzw.sYxzI67rA6CuLB.fMeoOLyaW2MLEZhmqNEbbCfyBv7C', 'OLZm19C2cSWu1vDelYPuK7MitwtlgswZsgos5GvHpxhiHGqnybfeSY2039By', '2021-08-15 03:38:29', '2021-08-15 03:39:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bobot_gap`
--
ALTER TABLE `bobot_gap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `hasil_pemilihan`
--
ALTER TABLE `hasil_pemilihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_produk`
--
ALTER TABLE `nilai_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_utility`
--
ALTER TABLE `nilai_utility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pemilihan_index`
--
ALTER TABLE `pemilihan_index`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pemilihan`
--
ALTER TABLE `tbl_pemilihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bobot_gap`
--
ALTER TABLE `bobot_gap`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hasil_pemilihan`
--
ALTER TABLE `hasil_pemilihan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `nilai_produk`
--
ALTER TABLE `nilai_produk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `nilai_utility`
--
ALTER TABLE `nilai_utility`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pemilihan_index`
--
ALTER TABLE `pemilihan_index`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_pemilihan`
--
ALTER TABLE `tbl_pemilihan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=431;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
